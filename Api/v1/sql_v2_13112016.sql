/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.6.17 : Database - bcode
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bcode` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bcode`;

/*Table structure for table `auth_session` */

DROP TABLE IF EXISTS `auth_session`;

CREATE TABLE `auth_session` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `permissionsFk` int(11) DEFAULT NULL,
  `rawToken` varchar(255) DEFAULT NULL,
  `channelAccountsFk` int(11) DEFAULT NULL,
  `channelCategory` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_session` */

/*Table structure for table `bcode` */

DROP TABLE IF EXISTS `bcode`;

CREATE TABLE `bcode` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `channelAccountFk` tinyint(1) DEFAULT NULL,
  `customersFK` int(255) DEFAULT NULL,
  `bcodeRef` varchar(255) DEFAULT NULL,
  `generationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `bcode` */

insert  into `bcode`(`id`,`channelAccountFk`,`customersFK`,`bcodeRef`,`generationDate`) values (1,2,1,'1393665653885139798','2016-11-02 12:16:03'),(2,2,2,'1393665653885139799','2016-11-02 12:21:53'),(3,2,3,'1393665653885139802','2016-11-02 12:29:44'),(4,2,4,'1393665653885139806','2016-11-02 12:37:01'),(5,2,5,'1393665653885139807','2016-11-02 12:41:08'),(6,2,6,'1393665653887365011','2016-11-11 14:01:33'),(7,2,7,'1393665653887365016','2016-11-11 14:03:08');

/*Table structure for table `bcode_lease_log` */

DROP TABLE IF EXISTS `bcode_lease_log`;

CREATE TABLE `bcode_lease_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lease` varchar(255) DEFAULT NULL,
  `bcodeFk` int(11) DEFAULT NULL,
  `generationDate` datetime DEFAULT NULL,
  `isExpired` tinyint(1) DEFAULT '0',
  `leaseStartDate` datetime DEFAULT NULL,
  `leaseEndDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `bcode_lease_log` */

insert  into `bcode_lease_log`(`id`,`lease`,`bcodeFk`,`generationDate`,`isExpired`,`leaseStartDate`,`leaseEndDate`) values (1,'0f2572e8ab7afb640b0613154a2c04ff',1,'2016-11-02 12:16:03',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(2,'e5118e47cfca918646783341be68c05a',2,'2016-11-02 12:21:53',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(3,'14f9410a95a4ac421481ff3fac05985e',3,'2016-11-02 12:29:44',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(4,'4b89f4c67af5afe195e1600138064117',4,'2016-11-02 12:37:01',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(5,'9aea003de3a091142fbf9e0413a76045',5,'2016-11-02 12:41:08',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(6,'08ab896c6e29c8f8542bdbe7340971ac',6,'2016-11-11 14:01:33',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(7,'d97c6d3429e36ee700737aa0f9040a12',7,'2016-11-11 14:03:08',0,'2016-10-01 00:00:00','2017-01-01 00:00:00');

/*Table structure for table `bcode_sms_log` */

DROP TABLE IF EXISTS `bcode_sms_log`;

CREATE TABLE `bcode_sms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sms` varchar(255) DEFAULT NULL,
  `smsRef` varchar(255) DEFAULT NULL,
  `bCodeFk` int(11) DEFAULT NULL,
  `generationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `bcode_sms_log` */

insert  into `bcode_sms_log`(`id`,`sms`,`smsRef`,`bCodeFk`,`generationDate`) values (1,'hey Tomjazz, here is your bCode','1393665653885226345',1,'2016-11-02 12:16:03'),(2,'hey Tomjazz, here is your bCode','1393665653885226448',2,'2016-11-02 12:21:53'),(3,'hey Tomjazz, here is your bCode','1393665653885226483',3,'2016-11-02 12:29:44'),(4,'hey Tomjazz, here is your bCode','1393665653885226619',4,'2016-11-02 12:37:01'),(5,'hey Tomjazz, here is your bCode','1393665653885226690',5,'2016-11-02 12:41:08'),(6,'message from bus002','1393665653887453963',1,'2016-11-11 13:51:01'),(7,'message from bus002 for more info on the usage of your unique bcode visit http://www.techadvance.ng','1393665653887454172',6,'2016-11-11 14:01:33'),(8,'message from bus002 for more info on the usage of your unique bcode visit http://www.techadvance.ng','1393665653887454185',7,'2016-11-11 14:03:08'),(9,'message from bus002','1393665653887454202',7,'2016-11-11 14:06:31'),(10,'message from bus002','1393665653887454437',1,'2016-11-11 14:13:16');

/*Table structure for table `channel_accounts` */

DROP TABLE IF EXISTS `channel_accounts`;

CREATE TABLE `channel_accounts` (
  `id` int(250) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `permissionsFk` int(11) DEFAULT NULL,
  `channelCategoryFk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `channel_accounts` */

insert  into `channel_accounts`(`id`,`username`,`password`,`permissionsFk`,`channelCategoryFk`) values (1,'tom','tom',1,1),(2,'bus','bus',2,2),(3,'tunde','tunde',1,1),(4,'edc','edc',2,2);

/*Table structure for table `channel_category` */

DROP TABLE IF EXISTS `channel_category`;

CREATE TABLE `channel_category` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `channel_category` */

insert  into `channel_category`(`id`,`name`) values (1,'all'),(2,'bus'),(3,'edc');

/*Table structure for table `channel_sub_category` */

DROP TABLE IF EXISTS `channel_sub_category`;

CREATE TABLE `channel_sub_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channelCategoryFk` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `channelSubcategoryCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `channel_sub_category` */

insert  into `channel_sub_category`(`id`,`channelCategoryFk`,`name`,`channelSubcategoryCode`) values (1,1,'All','admin001'),(2,2,'efex','bus001'),(3,2,'libra','bus002'),(4,3,'phedc','edc001');

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `customerName` varchar(250) DEFAULT NULL,
  `phone` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `customers` */

insert  into `customers`(`id`,`customerName`,`phone`) values (1,'Tomjazz','+2349094625113'),(2,'Tomjazz','+2347064985162'),(3,'Tomjazz','+2347000000002'),(4,'Tomjazz','+2349000000002'),(5,'Tomjazz','+2342000000002'),(6,'Wale','+2347036650660'),(7,'Wale','+2347036650669');

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `logs` */

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissions` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`permissions`) values (1,'Admin'),(2,'User');

/*Table structure for table `subscriber_ref_log` */

DROP TABLE IF EXISTS `subscriber_ref_log`;

CREATE TABLE `subscriber_ref_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriberRef` varchar(255) DEFAULT NULL,
  `bCodeFk` int(11) DEFAULT NULL,
  `generationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `subscriber_ref_log` */

insert  into `subscriber_ref_log`(`id`,`subscriberRef`,`bCodeFk`,`generationDate`) values (1,'1393665653879296094',1,'2016-11-02 12:16:03'),(2,'1393665653879331325',2,'2016-11-02 12:21:53'),(3,'1393665653885226482',3,'2016-11-02 12:29:44'),(4,'1393665653885226618',4,'2016-11-02 12:37:01'),(5,'1393665653885226689',5,'2016-11-02 12:41:08'),(6,'1393665653879296094',1,'2016-11-11 13:51:01'),(7,'1393665653887453979',6,'2016-11-11 14:01:33'),(8,'1393665653887454184',7,'2016-11-11 14:03:08'),(9,'1393665653887454184',7,'2016-11-11 14:06:31'),(10,'1393665653879296094',1,'2016-11-11 14:13:16');

/*Table structure for table `token` */

DROP TABLE IF EXISTS `token`;

CREATE TABLE `token` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `tokenString` varchar(255) DEFAULT NULL,
  `rawToken` varchar(255) DEFAULT NULL,
  `expiration` datetime DEFAULT NULL,
  `dateGenerated` datetime DEFAULT NULL,
  `isExpired` tinyint(1) DEFAULT NULL,
  `channelAccountsFk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `token` */

insert  into `token`(`id`,`tokenString`,`rawToken`,`expiration`,`dateGenerated`,`isExpired`,`channelAccountsFk`) values (1,'d86bd24836ad499ff041213d5df96527c0a9da91c614b2cf07472c207c04145b','22e183502291b14f743b6d40e979febb','2016-11-02 13:06:02','2016-11-02 12:06:02',0,2),(2,'2298caf2ca4dca337ac712b2fb1c1c8f38d2e0e11979f6c96e5eea956235b914','5225591b79f9b14bbb27025a17653b71','2016-11-02 13:46:57','2016-11-02 12:46:57',0,1),(3,'00d8aa573046625d5f0dc18fef645c21d35651fa49ad9f9ff047e8f6c106d1fe','b67adb270817c55f7ed20ca0b94a3ca8','2016-11-11 14:34:24','2016-11-11 13:34:24',0,2);

/*Table structure for table `transactions` */

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerFk` int(11) DEFAULT NULL,
  `bcodeFk` int(11) DEFAULT NULL,
  `leaseFk` int(11) DEFAULT NULL,
  `channelSubcategoryCode` varchar(255) DEFAULT NULL,
  `transactionRef` varchar(255) DEFAULT NULL,
  `transactionAmount` double DEFAULT NULL,
  `transactionDate` datetime DEFAULT NULL,
  `isUsed` tinyint(1) DEFAULT NULL,
  `dateUsed` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `transactions` */

insert  into `transactions`(`id`,`customerFk`,`bcodeFk`,`leaseFk`,`channelSubcategoryCode`,`transactionRef`,`transactionAmount`,`transactionDate`,`isUsed`,`dateUsed`) values (1,1,1,1,'bus002','trans666ff66f6382',3000,'2016-10-26 00:00:00',0,NULL),(2,2,NULL,2,'bus001','trans222222227ccc',3000,'2016-10-26 00:00:00',0,NULL),(3,2,2,2,'bus002','CCCC222222227ccc',3000,'2016-10-26 00:00:00',0,NULL),(4,3,NULL,3,'bus002','tstatdte3heheheeejejej',3000,'2016-10-26 00:00:00',0,NULL),(5,4,NULL,4,'bus002','hshshshshshshhsh',3000,'2016-10-26 00:00:00',0,NULL),(6,5,5,5,'bus002','DLLLSLDLDLDLDLLD',3000,'2016-10-26 00:00:00',0,NULL),(7,5,5,5,'bus002','dmsleygtuedi',3000,'2016-10-26 00:00:00',0,NULL),(8,1,1,1,'bus002','tRANS000000001',3000,'2016-11-11 13:51:01',0,NULL),(9,6,6,6,'bus002','transWales5577f',3000,'2016-11-11 14:01:33',0,NULL),(10,7,7,7,'bus002','Trans6g6f66ff6',3000,'2016-11-11 14:03:08',0,NULL),(11,7,7,7,'bus002','Trans6g6f66ff76',3000,'2016-11-11 14:06:31',0,NULL),(12,NULL,1,1,'bus002','Trans6g6f66f4fff76',3000,'2016-11-11 14:13:16',0,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
