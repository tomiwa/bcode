<?php
//wrote this comment on ubuntu distributed version of my project
include "RestEngine.php";
include "Repo.php";
include "ApiClient.php";
include "checknumber.php";

// Include and configure log4php
include('log4php/Logger.php');
Logger::configure('config.xml');

class Service extends RestEngine {


    /** Holds the Logger. */
    private $log;
    private $clientIpAddress;


    public function __construct() {
        parent::__construct();
        new Repo();
        // $this->dbConnect();
        // The __CLASS__ constant holds the class name (Service.)
        // Therefore this creates a logger named "Service" (which is configured in the config file)
        $this->log = Logger::getLogger(__CLASS__);
        $this->clientIpAddress = $_SERVER['REMOTE_ADDR'];
    }




    public function isRequestMethodPost($endPoint)
    {
        $genericBusinessLogicObj = new GenericBusinessLogic();
        $repoObj = new Repo();
        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params)? json_decode($input_params):$input_params;

        if ($this->get_request_method() == "POST") {
            return true;
        }else{

            $request =array('request'=>$obj);
            $response = array('response'=>array('code'=>405,'status' => 'false', 'message' => 'method not allowed, post method expected'));
            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
            $res =array_merge($request, $response);
            $this->log->error($this->clientIpAddress." ". "[". $endPoint."]".":- "."attempted to make a get request instead of post ");
            $this->response($genericBusinessLogicObj->json($res), 405);
        }
    }



    public function isInputJson($endPoint){

        $genericBusinessLogicObj = new GenericBusinessLogic();
        $repoObj = new Repo();

        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params)? json_decode($input_params):$input_params;

        if (strlen($input_params) > 0 && $genericBusinessLogicObj->isValidJSON($input_params)) {
            return true;
        } else {

            $request =array('request' =>$obj);
            $response =  array('response'=>array('code'=>403,'status' => 'false', 'message' => 'invalid Json request body'));
            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
            $res =array_merge($request, $response);
            $this->log->error($this->clientIpAddress." ". "[". $endPoint."]".":- "."sent a request body that is not in Json format");
            $this->response($genericBusinessLogicObj->json($res), 403);
            return false;

        }
    }



    public function isRequestParameterCountCompliant($reqCount, $endPoint)
    {
        $genericBusinessLogicObj = new GenericBusinessLogic();
        $repoObj = new Repo();
        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params)? json_decode($input_params):$input_params;

        if ($genericBusinessLogicObj->isRequestBodyCompliant($reqCount)) {
            return true;
        }else{

            $request =array('request'=>$obj);
            $response =  array('response'=>array('code'=>406,'status' => 'false', 'message' => 'Request body more or less than expected'));
            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
            $res =array_merge($request, $response);
            $this->log->error($this->clientIpAddress." ". "[". $endPoint."]".":- "."sent a request body more or less than expected");

            $this->response($genericBusinessLogicObj->json($res), 406);
            return false;
        }


    }




    public function isTokenExpired($token, $endPoint){
        $genericBusinessLogicObj = new GenericBusinessLogic();
        $repoObj = new Repo();
        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params)? json_decode($input_params):$input_params;


        if ($repoObj->isTokenExpired($token)) {


            $request =array('request'=>$obj);
            $response =  array('response'=>array('code'=>408,'status' => 'false', 'message' => 'session token expired, kindly re-authenticate'));
            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
            $res =array_merge($request, $response);
            $this->log->error($this->clientIpAddress." ". "[". $endPoint."]".":- "."token (".$token.") made a failed request with an expired token"." :- ". $token );
            $this->response($genericBusinessLogicObj->json($res), 408);
            return true;

        }else{

            return false;
        }
    }




    public function isUserAdmin($token, $endPoint){
        $repoObj = new Repo();
        $genericBusinessLogicObj = new GenericBusinessLogic();

        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params)? json_decode($input_params):$input_params;


        if ($repoObj->isUserAdmin($token)) {

            return true;

        }else{


            $request =array('request'=>$obj);
            $response =  array('response'=>array('code'=>401,'status' => 'false', 'message' => 'You dont have permission to perform this operation'));
            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
            $res =array_merge($request, $response);
            $this->log->error($this->clientIpAddress." ". "[". $endPoint."]".":- "."token (".$token.") attempted to perform unauthorized operation");
            $this->response($genericBusinessLogicObj->json($res), 401);
            return false;

        }

    }








    public function isUserAvailableForAuthentiation($username, $password, $channelSubcategoryCode,$endPoint ){

        $repoObj = new Repo();
        $genericBusinessLogicObj = new GenericBusinessLogic();

        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params)? json_decode($input_params):$input_params;

        if ($repoObj->userIsAvailable($username, $password, $channelSubcategoryCode)) {

            return true;

        } else {
            
            $request =array('request'=>$obj);
            $response =  array('response'=>array('code'=>404,'status' => 'false', 'message' => 'Username, password or channelSubcategoryCode does not exist'));
            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
            $res =array_merge($request, $response);
            $this->log->error($this->clientIpAddress." ". "[". $endPoint."]".":- "."attempted to authenticate through name (".$username.") but details could not be found in the DB");
            $this->response($genericBusinessLogicObj->json($res), 404);
            return false;
        }

    }




    public function insertNewChannelAccount($username, $password, $permissionsFk, $channelCategoryFk,$token, $endPoint ){

        $repoObj = new Repo();
        $genericBusinessLogicObj = new GenericBusinessLogic();


        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params)? json_decode($input_params):$input_params;


        if ($repoObj->isChannelAccountCreated($username, $password, $permissionsFk, $channelCategoryFk)) {

            $request =array('request'=>$obj);
            $response =  array('response'=>array('code'=>200,'status' => 'true', 'message' => 'channel user' . ' ' . $username . ' ' . 'successfully created'));
            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
            $res =array_merge($request, $response);
            $this->log->info($this->clientIpAddress." ". "[". $endPoint."]".":- "."user with token (".$token.")  successfully created user with name:- ".$username);


            $this->response($genericBusinessLogicObj->json($res), 200);
            return true;
        }else{
            return false;
        }

    }


    public function isUserRequestTokenValid($token, $endPoint){
        $repoObj = new Repo();
        $genericBusinessLogicObj = new GenericBusinessLogic();

        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params)? json_decode($input_params):$input_params;

        if ($repoObj->isUserRequestTokenValid($token)){
            return true;
        }  else{

            $request =array('request'=>$obj);
            $response = array('response'=> array('code'=>403,'status' => 'false', 'message' => 'Invalid Token Supplied'));
            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
            $res =array_merge($request, $response);
            $this->log->error($this->clientIpAddress." ". "[". $endPoint."]".":- "."made a request with an invalid token "." ". $token);
            $this->response($genericBusinessLogicObj->json($res), 403);
        }

    }



    public function isPhoneNumberValid($phonenumber){

        $cc = "234";
        $ph = ltrim($phonenumber, "0") ;

        if (checknumber($cc,$ph)) {
            return true;
        } else
        {
            return false;
        }



    }



    public function isAPIavailible ($rEndPoint, $endPoint){
        $genericBusinessLogicObj = new GenericBusinessLogic();
        $repoObj = new Repo();

        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params)? json_decode($input_params):$input_params;

        $apiClientObj = new ApiClient();
        if ($apiClientObj->isAPIavailible($rEndPoint)){
            return true;
        }else {

            $request =array('request'=>$obj);
            $response = array('response'=> array('code'=>500,'status' => 'false', 'message' => 'Internal Server Error'));
            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
            $res =array_merge($request, $response);
            $this->log->fatal($this->clientIpAddress." ". "[". $endPoint."]".":- "." connection to the bcode server could not be established  ");
            $this->response($genericBusinessLogicObj->json($res), 500);


        }
    }




    public function isbcodeExistForCustomer($customerPhone){

        $repoObj = new Repo();
        if ( $customerBcodeRefAndId = $repoObj->getCustomerbcodeRef($customerPhone)){

            return $customerBcodeRefAndId;

        }else{
            return false;
        }

    }




    public function auth()
    {
        $genericBusinessLogicObj = new GenericBusinessLogic();
        $endPoint ="bcode/Api/v1/Service/auth";
        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params);

        if ($this->isRequestMethodPost($input_params,$endPoint)) {

            if ($this->isInputJson($endPoint)) {

                if ($this->isRequestParameterCountCompliant(3, $endPoint, $input_params)) {

                   // $json = file_get_contents('php://input');
                   // $obj = json_decode($json);

                    $repoObj = new Repo();
                    if(property_exists($obj, "username") && property_exists($obj, "password") ){
                        $username = $obj->{'username'};
                        $password = $obj->{'password'};
                        $channelSubcategoryCode = $obj->{'channelSubcategoryCode'};


                        if (isset($username) && isset($password) && isset($channelSubcategoryCode)&& !empty($username) && !empty($password) && !empty($channelSubcategoryCode)) {


                            if ($this->isUserAvailableForAuthentiation($username, $password, $channelSubcategoryCode,$endPoint)) {


                                $rawToken = $genericBusinessLogicObj->generateToken($username, $password);
                                if ($repoObj->insertNewToken($rawToken, $repoObj->getIdOfLoggedOnUser($username, $password))) {

                                    //inserting Session Data
                                    // $repoObj->insertSessionData($username, $password, $repoObj->sessionChannelAccountsPermissionsId($username, $password), $rawToken, $repoObj->sessionChannelAccountsId($username, $password), $repoObj->sessionChannelAccountProductCatId($username, $password));

                                    //  if ($repoObj->isTokenExpired($rawToken) == false) {
                                    //    $repoObj->destroySessionData($rawToken);
                                    // }


                                    $request =array('request'=>$obj);
                                    $response = array('response'=>array('code'=>200,'status' => 'true', 'token' => $rawToken, 'message' => 'successfully authenticated.'));
                                    $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                    $this->log->info($this->clientIpAddress." [".$endPoint."] :- channel account"." ". $username." ". " successfully authenticated through". " ".$channelSubcategoryCode." ". "session Token :"." ". $rawToken);


                                    $res =array_merge($request, $response);
                                    $this->response($genericBusinessLogicObj->json($res), 200);
                                } else {



                                    $request =array('request'=>$obj);
                                    $response =  array('response'=> array('code'=>200,'status' => 'true', 'token' => $rawToken, 'message' => 'Your session is still valid.'));

                                    $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));


                                    $this->log->warn($this->clientIpAddress." [".$endPoint."] :- channel account"." ". $username." ". "with Token"." ".$rawToken." ". " tries to re-authenticate through". " ".$channelSubcategoryCode." ". "while session token is still valid");
                                    $res =array_merge($request, $response);
                                    $this->response($genericBusinessLogicObj->json($res), 200);
                                }

                            }


                        } else {

                            $request =array('request'=>$obj);
                            $response =  array('response'=>array('code'=>406,'status' => 'false', 'message' => 'one or more request parameter is not supplied'));
                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                            $res =array_merge($request, $response);

                            $this->log->error($this->clientIpAddress." [".$endPoint."] :- " . " Passed empty request body");
                            $this->response($genericBusinessLogicObj->json($res), 406);
                        }

                        // return true;

                    }else{
                        $request =array('request'=>$obj);
                        $response = array('response'=>array('code'=>406,'status' => 'false', 'message' => 'Unacceptable Request Parameters'));
                        $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                        $res =array_merge($request, $response);

                             $this->log->error($this->clientIpAddress." [".$endPoint."] :- " . " Passed Unacceptable Request Parameters");
                            $this->response($genericBusinessLogicObj->json($res), 406);
                        }

                    
                }

            }




        }

    }



    public function createChannelAccount()
    {
        $genericBusinessLogicObj = new GenericBusinessLogic();
        $repoObj = new Repo();
        $endPoint ="bcode/Api/v1/Service/createChannelAccount";
        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params);
        $token = $obj->{'token'};

        if ($this->isRequestMethodPost($input_params,$endPoint)) {

            if ($this->isInputJson($input_params,$endPoint)) {

                if ($this->isRequestParameterCountCompliant(5, $endPoint, $input_params)) {
                   // $json = file_get_contents('php://input');
                   // $obj = json_decode($json);

                    if (property_exists($obj, "token") && property_exists($obj, "username") && property_exists($obj, "password") && property_exists($obj, "permissionsFk") && property_exists($obj, "channelCategoryFk")) {

                        $token = $obj->{'token'};
                        $username = $obj->{'username'};
                        $password = $obj->{'password'};
                        $permissionsFk = $obj->{'permissionsFk'};
                        $channelCategoryFk = $obj->{'channelCategoryFk'};

                        if ($this->isUserRequestTokenValid($token, $endPoint, $input_params)) {

                            if ($this->isTokenExpired($token, $endPoint, $input_params) == false) {

                                if ($this->isUserAdmin($token, $endPoint, $input_params)) {

                                        if (isset($token) && isset($username) && isset($password) && isset($permissionsFk) && isset($channelCategoryFk) && !empty($token) && !empty($username) && !empty($username) && !empty($permissionsFk) && !empty($channelCategoryFk)) {

                                            if($permissionsFk ==1 && $channelCategoryFk !=1 || $channelCategoryFk==1  && $permissionsFk!=1){




                                                $request =array('request'=>$obj);
                                                $response =  array('response'=>array('code'=>406,'status' => 'false', 'message' => 'cannot assign admin permission to a non admin category'));
                                                $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                                $res =array_merge($request, $response);
                                                $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ")  tried assigning admin permission to a non admin category");
                                                $this->response($genericBusinessLogicObj->json($res), 406);


                                             }else {

                                                if ($repoObj->userIsInDatabase($username, $password, $channelCategoryFk) == false) {

                                                $this->insertNewChannelAccount($username, $password, $permissionsFk, $channelCategoryFk,$token, $endPoint,$input_params );

                                            } else {



                                                    $request =array('request'=>$obj);
                                                    $response =  array('response'=>array('code'=>406,'status' => 'false', 'message' => 'channel account already exist'));
                                                    $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                                    $res =array_merge($request, $response);
                                                    $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ")  tried creating an already existing channel account");
                                                    $this->response($genericBusinessLogicObj->json($res), 406);



                                            }


                                        }

                                    } else {


                                            $request =array('request'=>$obj);
                                            $response =  array('response'=>array('code'=>406,'status' => 'false', 'message' => 'one or more request parameter is not supplied'));
                                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                            $res =array_merge($request, $response);
                                            $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") tried creating an already existing channel account");
                                            $this->response($genericBusinessLogicObj->json($res), 406);



                                    }

                                }


                            }


                        }
                    }else{

                        $request =array('request'=>$obj);
                        $response =  array('response'=>array('code'=>406,'status' => 'false', 'message' => 'unacceptable Request parameters'));
                        $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                        $res =array_merge($request, $response);
                        $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ")  sent  invalid request parameters");
                        $this->response($genericBusinessLogicObj->json($res), 406);



                    }



                }

            }
        }

    }


    public function bcode()
    {
        $repoObj = new Repo();
        $apiClientObj = new ApiClient();
        $confObj = new __();
        $rEndPoint = $confObj->rootEndPoint();
        $genericBusinessLogicObj = new GenericBusinessLogic();


        $endPoint ="bcode/Api/v1/Service/bcode";
        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params);
        $token = $obj->{'token'};

        if ($this->isRequestMethodPost($input_params,$endPoint)) {


            if ($this->isAPIavailible ($rEndPoint, $endPoint, $input_params)) {


                if ($this->isInputJson($input_params,$endPoint)) {

                    if ($this->isRequestParameterCountCompliant(8, $endPoint, $input_params)) {
                       // $json = file_get_contents('php://input');
                       // $obj = json_decode($json);

                        if (property_exists($obj, "token") && property_exists($obj, "customerNumber") && property_exists($obj, "message")
                            && property_exists($obj, "customerName") && property_exists($obj, "transactionAmount")
                            && property_exists($obj, "channelSubcategoryCode") ) {

                            $token = $obj->{'token'};
                            $customerNumber = $obj->{'customerNumber'};
                            $customerName = $obj->{'customerName'};
                            $message = $obj->{'message'};
                            $transactionRef = $obj->{'transactionRef'};
                            $transactionAmount = $obj->{'transactionAmount'};
                            $channelSubcategoryCode = $obj->{'channelSubcategoryCode'};
                            $channelCategoryFk = $repoObj->returnProductChannelcategoryId($token);
                            $generationDate = new DateTime('NOW');
                            $generationDate = $generationDate->format('Y-m-d H:i:s');


                                if (isset($token) && isset($customerNumber) && isset($customerName) && isset($message) && isset($transactionRef)
                                && isset($transactionAmount) && isset($channelSubcategoryCode) && !empty($token) && !empty($customerNumber) && !empty($customerName) &&
                                !empty($message) && !empty($transactionRef) && !empty($transactionAmount)  && !empty($channelSubcategoryCode)
                            ) {

                                if ($this->isUserRequestTokenValid($token, $endPoint, $input_params)) {

                                    if ($this->isTokenExpired($token, $endPoint, $input_params) == false) {


                                        if($repoObj->isProductSubcategoryCodeValid($channelCategoryFk, $channelSubcategoryCode)){


                                            if ($this->isPhoneNumberValid($customerNumber)) {
                                            $customerNumber = "+234" . ltrim($customerNumber, "0");



                                            if ($repoObj->isTransactionExist($transactionRef)) {



                                                $request =array('request'=>$obj);
                                                $response =  array('response'=>array('code'=>403,'status' => 'false', 'message' => 'cannot post same transaction twice'));
                                                $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                                $res =array_merge($request, $response);
                                                $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") attempted to post an already existing transaction ");
                                                $this->response($genericBusinessLogicObj->json($res), 403);


                                            } else {


                                                if ($arr = $this->isbcodeExistForCustomer($customerNumber) == false) {

                                                    $message = $message .' '. 'for more info on the usage of your unique bcode visit http://www.techadvance.ng' ;
                                                    if ($bCodeAPIresponse = $apiClientObj->requestNewBcode($customerNumber, $message)) {


                                                        $bCodeRef = $bCodeAPIresponse['bcodeRef'];
                                                        $subscriberRef = $bCodeAPIresponse['subscriberRef'];
                                                        $smsRef = $bCodeAPIresponse['smsRef'];
                                                        $channelAccountsFk = $repoObj->returnChannelAccountsFk($token);


                                                        //Making a Request for bCODELease
                                                        $bCodeLeaseAPIresponse = $apiClientObj->requestBcodeLease($bCodeRef, $confObj->numberOfRequiredLease());


                                                        $lease = $bCodeLeaseAPIresponse['leaseCode'];
                                                        $leaseStartDate = $bCodeLeaseAPIresponse['leaseStartDate'];
                                                        $leaseEndDate = $bCodeLeaseAPIresponse['leaseEndDate'];



                                                        //inserting Customer
                                                        if ($repoObj->insertCustomer($customerName, $customerNumber) == true) {


                                                            //inserting bCode
                                                            $customersFk = $repoObj->returnCustomersFk($customerNumber);
                                                            $repoObj->insertbCode($channelAccountsFk, $customersFk, $bCodeRef, $generationDate);


                                                            //inserting Lease
                                                            $bcodeFk = $repoObj->returnbCodeFk($bCodeRef);

                                                            $repoObj->inserLease($lease, $bcodeFk, $generationDate, $leaseStartDate, $leaseEndDate);


                                                            //inserting subscriber
                                                            $bcodeFk = $repoObj->returnbCodeFk($bCodeRef);
                                                            $repoObj->insertSubscriberRef($subscriberRef, $bcodeFk, $generationDate);


                                                            //inserting Message
                                                            $bcodeFk = $repoObj->returnbCodeFk($bCodeRef);
                                                            $repoObj->insertSmsRef($message, $smsRef, $bcodeFk, $generationDate);


                                                            //inserting Transaction Details
                                                            $CustomerleaseFk = $repoObj->returnLeaseFk($bCodeRef);

                                                            $arr = $this->isbcodeExistForCustomer($customerNumber);
                                                            $customerbcodeFk = $arr[0];

                                                            if ($repoObj->isTransactionExist($transactionRef) == false) {

                                                                $repoObj->insertTransactionDetails($customersFk, $customerbcodeFk, $CustomerleaseFk, $channelSubcategoryCode, $transactionRef, $transactionAmount, $generationDate);

                                                            } else {


                                                                $response = array('code'=>403, 'status' => 'false', 'message' => 'cannot post same transaction twice');
                                                                $request =array('request'=>$obj);
                                                                $response = array('response'=>$response);
                                                                $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                                                $res =array_merge($request, $response);
                                                                $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") attempted to post same transaction twice");
                                                                $this->response($genericBusinessLogicObj->json($res), 403);


                                                            }


                                                            $response = array('code' => 200, 'status' => 'true', 'message' => 'bcode generated and sent to customer, new transaction record set for customer', 'bcodeRef' => $bCodeRef,  'transactionRef' => $transactionRef);
                                                            $request =array('request'=>$obj);
                                                            $response = array('response'=>$response);
                                                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                                            $res =array_merge($request, $response);
                                                            $this->log->info($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") generated bcode with Ref (". $bCodeRef .") for customer (".$customerNumber.")");
                                                            $this->response($genericBusinessLogicObj->json($res), 200);



                                                        }


                                                    } else {

                                                        $response = array('code'=>500,'status' => 'false', 'message' => 'bCODE Server Unavailable');
                                                        $request =array('request'=>$obj);
                                                        $response = array('response'=>$response);
                                                        $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                                        $res =array_merge($request, $response);
                                                        $this->log->fatal($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") could not establish a connection with the bcode server");
                                                        $this->response($genericBusinessLogicObj->json($res), 500);

                                                    }

                                                } else {


                                                    //**************** This block of code would be executed if the customer already has a bcode ***************************



                                                    //check if lease of the customer is at lease one week valid, if not then generate a lease for the customer as passing lease

                                                   $arr = $this->isbcodeExistForCustomer($customerNumber);

                                                    $returnedCustomerRef = $arr[1];
                                                    $bcodeFk = $repoObj->returnbCodeFk($returnedCustomerRef);


                                                    /**  if ( $repoObj->isCustomerLeaseMorethanOneWeekValid($returnedCustomerRef)==false ) {

                                                        $bcodeFk = $repoObj->returnbCodeFk($returnedCustomerRef);
                                                        $generationDate = new DateTime('NOW');
                                                        $generationDate = $generationDate->format('Y-m-d H:i:s');

                                                        $APIresponse = $apiClientObj->requestBcodeLease($returnedCustomerRef, 1);
                                                        $lease = $APIresponse[0];
                                                        $leaseStartDate = $APIresponse[1];
                                                        $leaseEndDate = $APIresponse[2];
                                                        $repoObj->inserLease($lease, $bcodeFk, $generationDate, $leaseStartDate, $leaseEndDate);

                                                    }
**/






                                                    //inserting Transaction Details
                                                    $customersFk = $repoObj->returnCustomersFk($customerNumber);
                                                    // $customerbCodeRef =$repoObj->getCustomerbcodeRef($customerNumber);
                                                    $arr = $this->isbcodeExistForCustomer($customerNumber);

                                                    $customerbcodeFk = $arr[0];
                                                   if($CustomerleaseFk = $repoObj->returnLeaseFk($returnedCustomerRef)){


                                                    $repoObj->insertTransactionDetails($customersFk, $customerbcodeFk, $CustomerleaseFk, $channelSubcategoryCode, $transactionRef, $transactionAmount, $generationDate);




                                                    $message = $message;
                                                    $arrResponse=$apiClientObj->sendSMS($customerNumber, $message);

                                                    $smsRef =$arrResponse['smsRef'];
                                                   // $subscriberRef=$arrResponse['subScriberRef'];

                                                    //inserting Message
                                                    $repoObj->insertSmsRef($message, $smsRef, $bcodeFk, $generationDate);




                                                       $response = array('code' => 200, 'status' => 'true', 'message' =>'bcode already exist for customer but new transaction record set', 'bcodeRef' => $returnedCustomerRef, 'transactionRef' => $transactionRef);
                                                       $request =array('request'=>$obj);
                                                       $response = array('response'=>$response);
                                                       $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                                       $res =array_merge($request, $response);
                                                       $this->log->info($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") initiated a new transaction record , 'bcodeRef' => $returnedCustomerRef, 'transactionRef' => $transactionRef ");
                                                       $this->response($genericBusinessLogicObj->json($res), 200);








                                                }

                                                }


                                            }


                                        } else {

                                                $response = array('code'=>406,'status' => 'false', 'message' => 'Invalid PhoneNumber Supplied');
                                                $request =array('request'=>$obj);
                                                $response = array('response'=>$response);
                                                $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                                $res =array_merge($request, $response);
                                                $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") did not supply a valid phone number ");
                                                $this->response($genericBusinessLogicObj->json($res), 406);

                                        }



                                    }else{


                                            $response = array('code'=>406,'status' => 'false', 'message' => 'invalid product sub category code supplied ');
                                            $request = array('request'=>$obj);
                                            $response = array('response'=>$response);
                                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                            $res = array_merge($request, $response);
                                            $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") did not supply a valid product sub category code ");
                                            $this->response($genericBusinessLogicObj->json($res), 406);


                                    }


                                    }
                                }

                            } else {


                                    $response = array('code'=>406,'status' => 'false', 'message' => 'one or more request parameter is not supplied');
                                    $request = array('request'=>$obj);
                                    $response = array('response'=>$response);
                                    $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                    $res = array_merge($request, $response);
                                    $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") did not supply complete request parameters ");
                                    $this->response($genericBusinessLogicObj->json($res), 406);


                            }



                                } else {


                            $response = array('code'=>406,'status' => 'false', 'message' => 'Unacceptable Request Parameters');
                            $request = array('request'=>$obj);
                            $response = array('response'=>$response);
                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                            $res = array_merge($request, $response);
                            $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") sent unacceptable request parameters");
                            $this->response($genericBusinessLogicObj->json($res), 406);


                                }


                            }

                        }

                    }
                }


            }







    public function retrieveCustomerbCode()
    {
        $repoObj = new Repo();
        $apiClientObj = new ApiClient();
        $confObj = new __();
        $rEndPoint = $confObj->rootEndPoint();
        $genericBusinessLogicObj = new GenericBusinessLogic();

        $endPoint ="bcode/Api/v1/Service/retrieveCustomerbCode";
        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params);
        $token = $obj->{'token'};

        if ($this->isRequestMethodPost($obj,$endPoint)) {

            if ($this->isAPIavailible ($rEndPoint, $endPoint, $input_params)) {


                if ($this->isInputJson($obj,$endPoint)) {

                    if ($this->isRequestParameterCountCompliant(2, $endPoint, $input_params)) {
                       // $json = file_get_contents('php://input');
                        //$obj = json_decode($json);

                        if (property_exists($obj, "token") && property_exists($obj, "customerNumber")) {
                            $token = $obj->{'token'};
                            $customerNumber = $obj->{'customerNumber'};

                            if (isset($token) && isset($customerNumber) && !empty($token) && !empty($customerNumber)) {

                                if ($this->isUserRequestTokenValid($token, $endPoint, $input_params)) {

                                    if ($this->isTokenExpired($token, $endPoint, $input_params) == false) {

                                        if ($this->isPhoneNumberValid($customerNumber)) {
                                            $customerNumber = "+234" . ltrim($customerNumber, "0");


                                            if($repoObj->isCustomerPhoneNumberExist($customerNumber)) {

                                                $customerBcodeRef = $repoObj->getCustomerbcodeRef($customerNumber);

                                                if($APIresponse =$apiClientObj->retrieveBcodeForCustomer($customerBcodeRef[1], $customerNumber)) {

                                                    $jsonArr = json_encode($APIresponse);
                                                    $this->response($jsonArr, 200);

                                                }else {
                                                    //echo "custNumber".$customerNumber;
                                                   // print_r($APIresponse); exit();

                                                    $response = array('code'=>500,'status' => 'false', 'message' => 'bCODE Server Unavailable');
                                                    $request = array('request'=>$obj);
                                                    $response = array('response'=>$response);
                                                    $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                                    $res = array_merge($request, $response);
                                                    $this->log->fatal($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") could not establish a connection with the bcode server");
                                                    $this->response($genericBusinessLogicObj->json($res), 500);

                                                }

                                            }else{

                                                $response =array('code'=>404,'status' => 'false', 'message' => 'phone number cannot be found');
                                                $request = array('request'=>$obj);
                                                $response = array('response'=>$response);
                                                $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                                $res = array_merge($request, $response);
                                                $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") verified customer phone ".$customerNumber." but could not be found");
                                                $this->response($genericBusinessLogicObj->json($res), 404);

                                            }

                                        }else {

                                            $response = array('code'=>406,'status' => 'false', 'message' => 'Invalid PhoneNumber Supplied');
                                            $request = array('request'=>$obj);
                                            $response = array('response'=>$response);
                                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                            $res = array_merge($request, $response);
                                            $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") verified customer phone ".$customerNumber." but could not be found");
                                            $this->response($genericBusinessLogicObj->json($res), 406);

                                        }

                                    }
                                }
                            }else{

                                $response = array('code'=>406,'status' => 'false', 'message' => 'one or more request parameter is not supplied');
                                $request = array('request'=>$obj);
                                $response = array('response'=>$response);
                                $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                $res = array_merge($request, $response);
                                $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") made a request with incomplete body");
                                $this->response($genericBusinessLogicObj->json($res), 406);


                            }
                        }else{

                            $response = array('code'=>406,'status' => 'false', 'message' => 'Unacceptable Request Parameters');
                            $request = array('request'=>$obj);
                            $response = array('response'=>$response);
                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                            $res = array_merge($request, $response);
                            $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") made a request with unacceptable request parameters");
                            $this->response($genericBusinessLogicObj->json($res), 406);



                        }
                    }
                }
            }
        }
    }








    public function verifyTransaction()
    {


        $repoObj = new Repo();
        $confObj = new __();
        $rEndPoint = $confObj->rootEndPoint();
        $genericBusinessLogicObj = new GenericBusinessLogic();

        $endPoint ="bcode/Api/v1/Service/verifyTransaction";
        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params);
        $token = $obj->{'token'};

        if ($this->isRequestMethodPost($endPoint)) {


            if ($this->isInputJson($endPoint)) {

                if ($this->isRequestParameterCountCompliant(3, $endPoint, $input_params)) {
                   // $json = file_get_contents('php://input');
                   // $obj = json_decode($json);

                    if (property_exists($obj, "token") && property_exists($obj, "lease") && property_exists($obj, "transactionRef")) {
                        $token = $obj->{'token'};
                        $lease = $obj->{'lease'};
                        $transactionRef = $obj->{'transactionRef'};

                        if (isset($token) && isset($lease) && isset($transactionRef) && !empty($token) && !empty($lease) && !empty($transactionRef)) {

                            if ($this->isUserRequestTokenValid($token, $endPoint, $input_params)) {

                                if ($this->isTokenExpired($token, $endPoint, $input_params) == false) {


                                    if($response= $repoObj->isTransactionvalidated($lease, $transactionRef)){

                                        $transactionRef = $response[0];
                                        $amount = $response[1];
                                        $transactionDate = $response[2];


                                        $response = array('code'=>200, 'status' => 'true', 'message' => 'transaction successfully validated', 'transactionRef' =>$transactionRef, 'amount'=>$amount, 'transactionDate' => $transactionDate);
                                        $request = array('request'=>$obj);
                                        $response = array('response'=>$response);
                                        $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                        $res = array_merge($request, $response);
                                        $this->log->info($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ")  successfully validated transaction with', 'transactionRef' =>$transactionRef, 'amount'=>$amount, 'transactionDate' => $transactionDate");
                                        $repoObj->updateStatusOfTransactionToUsed($transactionRef);
                                        $this->response($genericBusinessLogicObj->json($res), 200);


                                    }else{


                                        $response = array('code'=>406,'status' => 'false', 'message' => 'unable to validate transaction');
                                        $request = array('request'=>$obj);
                                        $response = array('response'=>$response);
                                        $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                        $res = array_merge($request, $response);
                                        $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ")  unable to validated transaction with', 'transactionRef' =>$transactionRef.");
                                        $this->response($genericBusinessLogicObj->json($res), 406);


                                    }



                                }


                            }


                        }else{


                            $response = array('code'=>406,'status' => 'false', 'message' => 'one or more request parameter is not supplied');
                            $request = array('request'=>$obj);
                            $response = array('response'=>$response);
                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                            $res = array_merge($request, $response);
                            $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ")  made a request with incomplete body");
                            $this->response($genericBusinessLogicObj->json($res), 406);


                        }



                    }else{

                        $response =  array('code'=>406,'status' => 'false', 'message' => 'Unacceptable Request Parameters');
                        $request = array('request'=>$obj);
                        $response = array('response'=>$response);
                        $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                        $res = array_merge($request, $response);
                        $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ")  made a request with unacceptable request parameters'");
                        $this->response($genericBusinessLogicObj->json($res), 406);

                    }
                }
            }

        }
    }






    public function CreateProductCategory()
    {
        $repoObj = new Repo();
        $genericBusinessLogicObj = new GenericBusinessLogic();

        $endPoint ="bcode/Api/v1/Service/CreateProductCategory";
        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params);
        $token = $obj->{'token'};


        if ($this->isRequestMethodPost($input_params,$endPoint)) {

            if ($this->isInputJson($input_params,$endPoint)) {

                if ($this->isRequestParameterCountCompliant(2, $endPoint, $input_params)) {
                   // $json = file_get_contents('php://input');
                   // $obj = json_decode($json);

                    if (property_exists($obj, "token") && property_exists($obj, "name")) {

                        $token = $obj->{'token'};
                        $name = $obj->{'name'};


                        if ($this->isUserRequestTokenValid($token, $endPoint, $input_params)) {

                            if ($this->isTokenExpired($token, $endPoint, $input_params) == false) {

                                if ($this->isUserAdmin($token, $endPoint, $input_params)) {

                                    if (isset($token) && isset($name) && !empty($token) && !empty($name) ) {


                                        if($repoObj->isproductCategoryAlreadyCreated($name) == false){
                                            if ($repoObj->createProductCategory($name)) {

                                                $response = array('code'=>200, 'status' => 'true', 'message' => 'Successfully Created', 'category name' =>$repoObj->returnChannelProductCategoryName($name), 'category id' =>$repoObj->returnChannelProductCategoryId($name));
                                                $request = array('request'=>$obj);
                                                $response = array('response'=>$response);
                                                $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                                $res = array_merge($request, $response);
                                                $this->log->info($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") Successfully Created', 'category name' => ".$repoObj->returnChannelProductCategoryName($name). ' category id => '.$repoObj->returnChannelProductCategoryId($name));
                                                $this->response($genericBusinessLogicObj->json($res), 200);


                                            }
                                        }else{


                                            $response = array('code'=>406,'status' => 'false', 'message' => 'channel category already exist', 'category name' =>$repoObj->returnChannelProductCategoryName($name), 'category id' =>$repoObj->returnChannelProductCategoryId($name));
                                            $request = array('request'=>$obj);
                                            $response = array('response'=>$response);
                                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                            $res = array_merge($request, $response);
                                            $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") attempted creating already existing channel category name category name' => ". $repoObj->returnChannelProductCategoryName($name). ', category id => '.$repoObj->returnChannelProductCategoryId($name));
                                            $this->response($genericBusinessLogicObj->json($res), 406);

                                        }

                                    } else {


                                        $response = array('code'=>406,'status' => 'false', 'message' => 'one or more request parameter is not supplied');
                                        $request = array('request'=>$obj);
                                        $response = array('response'=>$response);
                                        $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                        $res = array_merge($request, $response);
                                        $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") made a request with an incomplete body");
                                        $this->response($genericBusinessLogicObj->json($res), 406);

                                    }


                                }

                            }
                        }
                    }else{


                        $response =array('code'=>406,'status' => 'false', 'message' => 'Unacceptable Request Parameters');
                        $request = array('request'=>$obj);
                        $response = array('response'=>$response);
                        $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                        $res = array_merge($request, $response);
                        $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") made a request with unacceptable request parameters");
                        $this->response($genericBusinessLogicObj->json($res), 406);

                    }
                }
            }
        }
    }






    public function getAllproductCategory()
    {
        $repoObj = new Repo();
        $genericBusinessLogicObj = new GenericBusinessLogic();

        $endPoint ="bcode/Api/v1/Service/getAllproductCategory";
        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params);
        $token = $obj->{'token'};


        if ($this->isRequestMethodPost($input_params,$endPoint)) {

            if ($this->isInputJson($input_params,$endPoint)) {

                if ($this->isRequestParameterCountCompliant(1, $endPoint, $input_params)) {
                   // $json = file_get_contents('php://input');
                   // $obj = json_decode($json);

                    if (property_exists($obj, "token")) {

                        $token = $obj->{'token'};


                        if ($this->isUserRequestTokenValid($token, $endPoint, $input_params)) {

                            if ($this->isTokenExpired($token, $endPoint, $input_params) == false) {

                                if ($this->isUserAdmin($token, $endPoint, $input_params)) {

                                    if (isset($token) && !empty($token)) {




                                        if($arrResponse =$repoObj->returnAllProductCategory()) {



                                            $response = $arrResponse;
                                            $request  = array('request'=>$obj);
                                            $response = array('response'=>$response);
                                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                            $res = array_merge($request, $response);
                                            $this->log->info($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") successfully  made a request to view all product  categories'");
                                            $this->response($genericBusinessLogicObj->json($res), 200);



                                        }else{


                                            $response = array('code'=>404,'status' => 'false', 'message' => 'product category not yet setup');
                                            $request = array('request'=>$obj);
                                            $response = array('response'=>$response);
                                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                            $res = array_merge($request, $response);
                                            $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") made a request to view all product category but none was found");
                                            $this->response($genericBusinessLogicObj->json($res), 404);


                                        }

                                    } else {

                                        $response = array('code'=>406,'status' => 'false', 'message' => 'one or more request parameter is not supplied');
                                        $request = array('request'=>$obj);
                                        $response = array('response'=>$response);
                                        $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                        $res = array_merge($request, $response);
                                        $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") made a request with an incomplete body");
                                        $this->response($genericBusinessLogicObj->json($res), 406);

                                    }
                                }
                            }
                        }
                    }else {

                        $response = array('code'=>406,'status' => 'false', 'message' => 'Unacceptable Request Parameters');
                        $request  = array('request'=>$obj);
                        $response = array('response'=>$response);
                        $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                        $res = array_merge($request, $response);
                        $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") made a request with unacceptable request parameters'");
                        $this->response($genericBusinessLogicObj->json($res), 406);

                    }
                }
            }
        }

    }








    public function getAllproductSubCategory()
    {
        $repoObj = new Repo();
        $genericBusinessLogicObj = new GenericBusinessLogic();

        $endPoint ="bcode/Api/v1/Service/getAllproductSubCategory";
        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params);
        $token = $obj->{'token'};


        if ($this->isRequestMethodPost($input_params,$endPoint)) {

            if ($this->isInputJson($input_params,$endPoint)) {

                if ($this->isRequestParameterCountCompliant(1, $endPoint, $input_params)) {
                   // $json = file_get_contents('php://input');
                    //$obj = json_decode($json);

                    if (property_exists($obj, "token")) {

                        $token = $obj->{'token'};


                        if ($this->isUserRequestTokenValid($token, $endPoint, $input_params)) {

                            if ($this->isTokenExpired($token, $endPoint, $input_params) == false) {

                                if ($this->isUserAdmin($token, $endPoint, $input_params)) {

                                    if (isset($token) && !empty($token)) {




                                        if($jsonResponse =$repoObj->returnAllProductSubCategory()) {



                                            $response = $jsonResponse;
                                            $request  = array('request'=>$obj);
                                            $response = array('response'=>$response);
                                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                            $res = array_merge($request, $response);
                                            $this->log->info($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") successfully  made a request to view all product sub categories'");
                                            $this->response($genericBusinessLogicObj->json($res), 200);




                                        }else{




                                            $response = array('code'=>404,'status' => 'false', 'message' => 'product subcategory not yet setup');
                                            $request  = array('request'=>$obj);
                                            $response = array('response'=>$response);
                                            $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                            $res = array_merge($request, $response);
                                            $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") made a request to view all product sub categories but none was found");
                                            $this->response($genericBusinessLogicObj->json($res), 404);



                                        }

                                    } else {


                                        $response = array('code'=>406,'status' => 'false', 'message' => 'one or more request parameter is not supplied');
                                        $request  = array('request'=>$obj);
                                        $response = array('response'=>$response);
                                        $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                        $res = array_merge($request, $response);
                                        $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") made a request with an incomplete body");
                                        $this->response($genericBusinessLogicObj->json($res), 406);


                                    }
                                }
                            }
                        }
                    }else {

                        $response =  array('code'=>406,'status' => 'false', 'message' => 'Unacceptable Request Parameters');
                        $request  = array('request'=>$obj);
                        $response = array('response'=>$response);
                        $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                        $res = array_merge($request, $response);
                        $this->log->error($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") made a request with unacceptable request parameters");
                        $this->response($genericBusinessLogicObj->json($res), 406);

                    }
                }
            }
        }

    }







    public function CreateChannelProductSubCategory()
    {
        $repoObj = new Repo();
        $genericBusinessLogicObj = new GenericBusinessLogic();
        $endPoint ="bcode/Api/v1/Service/CreateChannelProductSubCategory";
        $input_params = file_get_contents('php://input');
        $obj = json_decode($input_params);

        if ($this->isRequestMethodPost($obj,$endPoint)) {

            if ($this->isInputJson($obj,$endPoint)) {

                if ($this->isRequestParameterCountCompliant(4, $endPoint, $input_params)) {
                 //   $json = file_get_contents('php://input');
                  //  $obj = json_decode($json);

                    if (property_exists($obj, "token") && property_exists($obj, "channelSubCategoryName") && property_exists($obj, "channelCategoryFk") && property_exists($obj, "channelSubcategoryCode")) {

                        $token = $obj->{'token'};
                        $channelSubCategoryName = $obj->{'channelSubCategoryName'};
                        $channelCategoryFk = $obj->{'channelCategoryFk'};
                        $channelSubcategoryCode = $obj->{'channelSubcategoryCode'};



                        if ($this->isUserRequestTokenValid($token, $endPoint, $input_params)) {

                            if ($this->isTokenExpired($token, $endPoint, $input_params) == false) {

                                if ($this->isUserAdmin($token, $endPoint, $input_params)) {

                                    if (isset($token) && isset($channelSubCategoryName) && isset($channelCategoryFk) && isset($channelSubcategoryCode)  && !empty($token) && !empty($channelSubCategoryName) && !empty($channelCategoryFk)  && !empty($channelSubcategoryCode) ) {

                                        if ($repoObj->returnProductCategoryId($channelCategoryFk)){


                                            if ($repoObj->isproductSubCategoryAlreadyCreated($channelSubCategoryName, $channelCategoryFk, $channelSubcategoryCode) == false) {

                                                if ($repoObj->createProductSubCategory($channelSubCategoryName, $channelCategoryFk, $channelSubcategoryCode)) {

                                                    $response =  array('code'=>200, 'status' => 'true', 'message' => 'Channel Subcategory' . ' ' . $channelSubCategoryName . ' ' . 'Successfully Created', 'category assigned' => $repoObj->returnProductCategoryName($channelCategoryFk), 'category id' => $repoObj->returnProductCategoryId($channelCategoryFk));
                                                    $request  = array('request'=>$obj);
                                                    $response = array('response'=>$response);
                                                    $repoObj->apiActivityLoggin($endPoint, json_encode($request), json_encode($response));
                                                    $res = array_merge($request, $response);
                                                    $this->log->info($this->clientIpAddress." ". "[".$endPoint."] :- " . "account with token (" .$token. ") successfully  made a request to view all product sub categories'");
                                                    $this->response($genericBusinessLogicObj->json($res), 200);


                                                }





                                            } else {
                                                $error = array('code'=>406,'status' => 'false', 'message' => 'similar record found for account, try again' );
                                                $this->response($genericBusinessLogicObj->json($error), 406);
                                            }


                                    }else{

                                            $error = array('code'=>404,'status' => 'false', 'message' => 'channel category id provided does not exist' );
                                            $this->response($genericBusinessLogicObj->json($error), 404);


                                    }

                                    } else {
                                        $error = array('code'=>406,'status' => 'false', 'message' => 'one or more request parameter is not supplied');
                                        $this->response($genericBusinessLogicObj->json($error), 406);
                                    }


                                }

                            }
                        }
                    }else{
                        $error = array('code'=>406,'status' => 'false', 'message' => 'Unacceptable Request Parameters');
                        $this->response($genericBusinessLogicObj->json($error), 406);
                    }
                }
            }
        }
    }





    public function decodeLease(){
        foreach ($_GET as $key=>$value) {
            echo "$key = " . urldecode($value) . "<br />\n";
            $this->log->info("$key = " . urldecode($value) . "<br />\n");

        }

        foreach (getallheaders() as $name => $value) {
            echo "$name: $value\n";
            $this->log->info("$key = " . urldecode( "$name: $value\n") . "<br />\n");
        }
    }


}

$api = new Service();
$api->processApi();
?>
