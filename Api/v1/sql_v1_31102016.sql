/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.6.17 : Database - bcode
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bcode` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bcode`;

/*Table structure for table `auth_session` */

DROP TABLE IF EXISTS `auth_session`;

CREATE TABLE `auth_session` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `permissionsFk` int(11) DEFAULT NULL,
  `rawToken` varchar(255) DEFAULT NULL,
  `channelAccountsFk` int(11) DEFAULT NULL,
  `channelCategory` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_session` */

/*Table structure for table `bcode` */

DROP TABLE IF EXISTS `bcode`;

CREATE TABLE `bcode` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `channelAccountFk` tinyint(1) DEFAULT NULL,
  `customersFK` int(255) DEFAULT NULL,
  `bcodeRef` varchar(255) DEFAULT NULL,
  `generationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `bcode` */

insert  into `bcode`(`id`,`channelAccountFk`,`customersFK`,`bcodeRef`,`generationDate`) values (22,11,25,'1393665653884444402','2016-10-26 11:42:18'),(23,11,26,'1393665653884444404','2016-10-26 11:43:38'),(24,11,27,'1393665653884445439','2016-10-26 13:29:28');

/*Table structure for table `bcode_lease_log` */

DROP TABLE IF EXISTS `bcode_lease_log`;

CREATE TABLE `bcode_lease_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lease` varchar(255) DEFAULT NULL,
  `bcodeFk` int(11) DEFAULT NULL,
  `generationDate` datetime DEFAULT NULL,
  `isExpired` tinyint(1) DEFAULT '0',
  `leaseStartDate` datetime DEFAULT NULL,
  `leaseEndDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `bcode_lease_log` */

insert  into `bcode_lease_log`(`id`,`lease`,`bcodeFk`,`generationDate`,`isExpired`,`leaseStartDate`,`leaseEndDate`) values (21,'867fc0093f7111a9a259124edbe538b9',22,'2016-10-26 11:42:18',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(22,'04486aab71a5d20c2005d51774dd3fad',23,'2016-10-26 11:43:38',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(23,'cff6fccddafa624c23677f47a1df3ac5',24,'2016-10-26 13:29:28',0,'2016-10-01 00:00:00','2017-01-01 00:00:00');

/*Table structure for table `bcode_sms_log` */

DROP TABLE IF EXISTS `bcode_sms_log`;

CREATE TABLE `bcode_sms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sms` varchar(255) DEFAULT NULL,
  `smsRef` varchar(255) DEFAULT NULL,
  `bCodeFk` int(11) DEFAULT NULL,
  `generationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `bcode_sms_log` */

insert  into `bcode_sms_log`(`id`,`sms`,`smsRef`,`bCodeFk`,`generationDate`) values (15,'hey Tomiwa, here is your bCode','1393665653884594553',22,'2016-10-26 11:42:18'),(16,'hey Tomjazz, here is your bCode','1393665653884594565',23,'2016-10-26 11:43:38'),(17,'Hey Tunde, above is your bCode','1393665653884611183',24,'2016-10-26 13:29:28');

/*Table structure for table `channel_accounts` */

DROP TABLE IF EXISTS `channel_accounts`;

CREATE TABLE `channel_accounts` (
  `id` int(250) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `permissionsFk` int(11) DEFAULT NULL,
  `channelAccountSubCategoryFk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `channel_accounts` */

insert  into `channel_accounts`(`id`,`username`,`password`,`permissionsFk`,`channelAccountSubCategoryFk`) values (1,'tom','tom',1,1),(11,'efex','efex',2,3);

/*Table structure for table `channel_category` */

DROP TABLE IF EXISTS `channel_category`;

CREATE TABLE `channel_category` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `channel_category` */

insert  into `channel_category`(`id`,`name`) values (1,'all'),(2,'bus'),(3,'edc'),(5,'libra');

/*Table structure for table `channel_sub_category` */

DROP TABLE IF EXISTS `channel_sub_category`;

CREATE TABLE `channel_sub_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `channelCategoryFk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `channel_sub_category` */

insert  into `channel_sub_category`(`id`,`name`,`channelCategoryFk`) values (1,'all',1),(2,'libra',2),(3,'efex',2);

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `customerName` varchar(250) DEFAULT NULL,
  `phone` varchar(250) DEFAULT NULL,
  `channelSubcategoryFk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Data for the table `customers` */

insert  into `customers`(`id`,`customerName`,`phone`,`channelSubcategoryFk`) values (25,'Tomiwa Adebayo','+2347064985162',3),(26,'Tomjazz','+2349094625113',3),(27,'Tunde','+2348181699312',3);

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `logs` */

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissions` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`permissions`) values (1,'Admin'),(2,'User');

/*Table structure for table `subscriber_ref_log` */

DROP TABLE IF EXISTS `subscriber_ref_log`;

CREATE TABLE `subscriber_ref_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriberRef` varchar(255) DEFAULT NULL,
  `bCodeFk` int(11) DEFAULT NULL,
  `generationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `subscriber_ref_log` */

insert  into `subscriber_ref_log`(`id`,`subscriberRef`,`bCodeFk`,`generationDate`) values (18,'1393665653879331325',22,'2016-10-26 11:42:18'),(19,'1393665653879296094',23,'2016-10-26 11:43:38'),(20,'1393665653883573857',24,'2016-10-26 13:29:28');

/*Table structure for table `token` */

DROP TABLE IF EXISTS `token`;

CREATE TABLE `token` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `tokenString` varchar(255) DEFAULT NULL,
  `rawToken` varchar(255) DEFAULT NULL,
  `expiration` datetime DEFAULT NULL,
  `dateGenerated` datetime DEFAULT NULL,
  `isExpired` tinyint(1) DEFAULT NULL,
  `channelAccountsFk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=latin1;

/*Data for the table `token` */

insert  into `token`(`id`,`tokenString`,`rawToken`,`expiration`,`dateGenerated`,`isExpired`,`channelAccountsFk`) values (138,'d0c0a442c965ce6b2a43686c8c970b9841cbb69b22785b3074b996e86110f9ca','44b89b997047c764d8a7fa1b9070b747','2016-10-15 06:28:39','2016-10-15 06:28:19',1,2),(139,'a0677ffbdc94ead131cc3b515541276e9661452fb8d8f03ee335348ae46c41c7','1501c8688028e24e5d4b486bdd710dae','2016-10-16 22:53:57','2016-10-16 22:53:37',1,2),(140,'d9ebdd51e852248183efe9eee558dcfc64bd42e2a8d0c230c100384d03601c67','2baf49dcf0216afdca8f35c7faeac550','2016-10-17 01:00:51','2016-10-17 01:00:31',1,2),(141,'44fd95be90fc0c3b0ab8c8af983eb07992d78c847034c8ed701e1def41d6b920','5b00d2544c391d874b136fa210de0df8','2016-10-17 01:50:30','2016-10-17 01:20:30',1,2),(142,'277e39da7440d18b8df431ddc41b354e5383ebad27735a09891d05d2f08e8f03','e1e506cfb65462346879a2a8f471d7e3','2016-10-17 11:03:07','2016-10-17 10:33:07',1,2),(143,'62b1430758c6b819a985708c2a40d3ce5528d7be9da25126106e3a58a4b69396','a52fb591dfebbea7d9b0a1ec7855ec86','2016-10-17 11:34:51','2016-10-17 11:04:51',1,2),(144,'03e0fc7d1903d313fd3cadefdd97c4c4d4877c0198f404e8d6db703db7705679','3ce6c27e5c34da8333920476cd405846','2016-10-17 12:31:45','2016-10-17 12:01:45',1,2),(145,'f56effdc832ae41dae3ad422a78022ccdf1aca362a4fb39871a5305b105aaea8','501de74d5f2a1fdf898e72d0c9e83e31','2016-10-17 15:24:12','2016-10-17 14:54:12',1,1),(146,'bf6967b6c8eca35be3c15d7b845d3a6c719536de8a8aec6a3fb03e17291cb9f9','c2941d0046d52a2c94d8d167d6b11539','2016-10-17 15:38:45','2016-10-17 15:37:45',1,1),(147,'0895fb19031c34c5daceb90b5c00286e304b33bdd00fe58adf25b98c140f0cbe','6ef76367123d70d1790dad2b3c66d634','2016-10-17 16:29:54','2016-10-17 16:28:54',1,1),(148,'7a587abb945073f219ba5b16e0826d81c0b3cbd09d10d137459eb73b52f57db8','31e60b6484e0c48e1502084e5d5dc0ec','2016-10-17 17:16:27','2016-10-17 17:15:27',1,1),(149,'6b71aad4e7d4fa0b2572762f075c1666c2b48c7b16776244cdccdd93c7ca1da9','ec36ef35b7a4b930447678759fba3adb','2016-10-17 17:18:06','2016-10-17 17:17:06',1,1),(150,'ee5efef849601f09d3952ac02b7b6e80369288dfae966d3a632e4e0a6fc3efaa','81a79fce369321c43efadbeaae8d2e96','2016-10-17 17:19:08','2016-10-17 17:18:08',1,6),(151,'46025e1bc4d827beeb495a409b73e536d3d158b97d2e4a153d38e61a5c3158db','64f4f891ac88122d87196419d32c74ae','2016-10-17 17:20:12','2016-10-17 17:19:12',1,6),(152,'b73b9ff2387a57247d932c2468c1e8812b88bbc9c2ae8bbc9694714008753c27','be3e88cd93ef24d4641e71ce3a29029f','2016-10-18 11:15:17','2016-10-18 11:14:17',0,6),(153,'47eaf65ff28644cfda4951d227b827a338018014438f56f33797ad0e55db29d5','a8b98cefca558c95bbfcbe73ac2dac87','2016-10-18 11:16:27','2016-10-18 11:15:27',0,6),(154,'06f1e2c34a512a1848bde095aafba1e56650e492f09e581ea74a3d8a66c4096e','0f04e24b0636b5dd3d09d4627d0833d0','2016-10-18 11:50:34','2016-10-18 11:49:34',0,6),(155,'333a0a2dc185c1c379da90490ac0156aa4112eb2265c2775ca539ced98f0149c','cd606b2d07e85ae20d87c97f8e1b5d4b','2016-10-18 11:55:04','2016-10-18 11:54:04',0,6),(156,'10b09626a6e9e5148f3144f72a66a514f6a9035597269c83e3f63310c063e123','13e48035a5739cd10c49eafc5ba6e55e','2016-10-18 11:56:09','2016-10-18 11:55:09',0,6),(157,'53cd7cc34ce33b6d1fc1251a6a11df5ba80a70efe8a3484583b4cffb4c00b019','b2be0e741c33736e27918b9fdba5bd05','2016-10-18 11:58:09','2016-10-18 11:57:09',0,6),(158,'64d30fccc10b7c346d21f83a5ec856ed40cd7cdb8ad40fdb10acc341da0bdee2','7b1f2671c767b75b0f89afbde622f67a','2016-10-18 12:03:57','2016-10-18 12:02:57',0,6),(159,'b0a92a12b25dbb72796c81eaddcf1e2c12a40eb2f6b5d131883627de16dc1927','b32f3810cb058909d4d453eaa7e8cf31','2016-10-18 12:08:21','2016-10-18 12:07:21',0,6),(160,'4b00c2e1b369bfb17b94043222775b40d71c293f5453988f732632dbfc6dd00b','4b79daddd6e249b371ff06858d82c9f3','2016-10-18 12:30:08','2016-10-18 12:29:08',0,6),(161,'0a140e8e2832ee3e28a57351b3c180ecbf4ac86bbfc36b3447610ae75be2f2c9','41549bed157bff985e83c63156c991c8','2016-10-18 12:33:22','2016-10-18 12:32:22',0,6),(162,'66fe8f66bcb831f7b3c7fa3f9cdc83b968ba3197b8a48cbde49d70b4f8359861','27edd933b93dc95fcc4475e0005a0846','2016-10-18 13:35:48','2016-10-18 12:35:48',0,6),(163,'4cf06f23d7f6349bec0fd8f294d247e3b4e0fa8b85045ffc9915fbbfba0807e1','71ef0fbda2649c5c51b7674bb00606a8','2016-10-18 14:34:54','2016-10-18 13:34:54',0,1),(164,'5dd7507d9d3423e5c22cf031393c20f123bf9e5e30bc04b93f01f0a3224b6063','12da03ffb60b2cc7f2c32e10cb11649d','2016-10-18 15:52:33','2016-10-18 14:52:33',0,1),(165,'bd9e09fb621b328b9338ef847bd2e9e0d9ce70b5825463a3a7debe55afff4883','3575bd9aa21222cbdd4aed71858ce539','2016-10-18 18:29:55','2016-10-18 17:29:55',0,1),(166,'8778157027f3697d6ec659bda5f153e2a4db8cedda321a05e42599e46b043f29','52d445ce87782e092bc28de9998d0060','2016-10-18 18:30:23','2016-10-18 17:30:23',0,2),(167,'8f00f5c8838ff6de5c760aeff6010579ab1bf1c0b86d5ec3e0022d00b4afe90c','4d7fa3cbe41432555bb8bd46a1973fa8','2016-10-19 01:29:14','2016-10-19 00:29:14',0,1),(168,'53e3a261a47521dc3092920a772d7153a39ebaa11b6a9ed8233395a64378e178','15fc25e5a31f65eebd49fddd8d324c53','2016-10-19 11:20:15','2016-10-19 10:20:15',0,1),(169,'e22db6226854ee904e1113cda384ee80248b3ade8c07865cbc0d0b209d8459ec','be13f98fb48febbe9c09704afa8ebc5a','2016-10-19 16:04:02','2016-10-19 15:04:02',0,1),(170,'7367a6098115871fcb519b04c8ba93b4560ff20086e37473fd6876ebf0dcd975','187a5c4f3155489bf9f7684689647710','2016-10-19 17:13:19','2016-10-19 16:13:19',0,10),(171,'838103b79a0236f5eff184936bfdc0bcc282c178f76a6f0371d9d289cdeab8da','1fe126159536748c756d85275d50041d','2016-10-19 19:13:16','2016-10-19 18:13:16',0,10),(172,'95c9de6a700c7b90ec734cb4c7e841c117ea885274f328a1c2bf4cb8108a8e3e','a48048542b47fb66944420afa9666fd8','2016-10-23 15:43:19','2016-10-23 14:43:19',0,10),(173,'3d8fd79f36e4dea17494aaf59b170aaf8ef33ffd8907e5d4f4ba0909714c9795','b1547ed87f524e2ea2ad46fe1734d51c','2016-10-24 00:53:12','2016-10-23 23:53:12',0,10),(174,'5042e0f7be53089aed564d1067100d139b795e4d2e269fb1e434f87480eb63cf','dba66c055db4d1ea5caad4caef213103','2016-10-24 01:29:08','2016-10-24 00:29:08',0,1),(175,'f5ba4c69523522331962aa087711d7fe437ec3f17bc60ab2b857941e61d2e2c3','29bb487883233aec7d3dbe27fdc47b69','2016-10-24 02:58:17','2016-10-24 01:58:17',0,1),(176,'7a0219294d5b6f77170e96137a37516dd29888884a4946f3af790d54d93abaaa','381208e1a883e5612e8f285c8de406c4','2016-10-24 03:27:17','2016-10-24 02:27:17',0,11),(177,'98c1f3bb475f551226516b952435083d3d54db1860fad20859c6678ededb56ea','7740ab8cb89eda423bfb8b8ae2f3064e','2016-10-24 12:26:03','2016-10-24 11:26:03',0,1),(178,'37cce2f25704ec211a39555d14023fdf9df393a041956f6042743bf9af634478','5dfc268c69ed280a8cbe0cb37384cc09','2016-10-24 13:26:18','2016-10-24 12:26:18',0,1),(179,'b0259fd87fbe03d735d46c9ff6b80e5d8bc3659971256271c9d110e23c9aa3a0','6e63b418c7ae099e49329c82811003d9','2016-10-24 13:59:46','2016-10-24 12:59:46',0,11),(180,'9658576b80f3eb7204f7a827f5c7c1e5b7d4a87a729605711928aff0752df10f','9d1c83da396454af05558c99907c1fa4','2016-10-24 17:50:38','2016-10-24 16:50:38',0,11),(181,'3fc765b545cbeace6c4e608b6c4ae00cd7276342cb63e987f0fc88c0480c695c','1496f01cbd71cdc721cc16742d926160','2016-10-24 18:51:18','2016-10-24 17:51:18',0,11),(182,'1e9f828a70ec8054436728cfd78a59fabe91197f102ebcc62255d62db49bff7d','28a123e35dae44f92198079bed8d43d4','2016-10-25 12:49:52','2016-10-25 11:49:52',0,11),(183,'3b37119235c1952988fd2990d4eeda9e533cef719145a55e69a8ac2e51137d3c','cb8dd75413e1576836a917bd1f7d8587','2016-10-25 19:38:56','2016-10-25 18:38:56',0,11),(184,'15b1d5b5bd2ac21452e2f82c13ffb5c8c9eff359969ddb3e91c5167c8e61e979','c5bbafd302af01c9bb2db1b1352e6cbf','2016-10-25 19:54:05','2016-10-25 18:54:05',0,1),(185,'4590783b4cb2692285685937103a2d8dc8cd770304118a2ae60d63ad6cf40c59','1ab11d6bbaae4f6c7ea60f3824cee15a','2016-10-26 11:30:34','2016-10-26 10:30:34',0,1),(186,'5d6c873f8364ec44bedf3d694e48e80a8d764613e24b4e5a17ed31e1783830a9','a81a5a5daad074abbf48aec0e23da3c4','2016-10-26 12:32:57','2016-10-26 11:32:57',0,11),(187,'fa3ffbb53c5653d8089bddad00b19fbc2d2db94b32dbff272a1a7320ab15a7b8','b2a7fbb89ae70b52da3e90f953712cb2','2016-10-26 13:43:43','2016-10-26 12:43:43',0,11),(188,'16ab4e5267e0bddb36833b6396c8b7f37bdf97c6a004192ddf8004d31a618c8f','5129281667b401eb54fab879b1af521d','2016-10-26 14:04:03','2016-10-26 13:04:03',0,1),(189,'d460f32d51c1bd150c3dabb9b470f94449cc9787999195f46ac983b16ac7cf33','4ce8962de5fe930d73a2fc6eb81903eb','2016-10-26 15:07:40','2016-10-26 14:07:40',0,11),(190,'be8992a5a1060168c59320e8c25ad46bc45d1cf06f87fb798d253cc1038d3f68','7355a406f618648fe514bb46e3f92bb5','2016-10-31 04:46:25','2016-10-31 03:46:25',0,11),(191,'b6b664e6e1d4255593c2539b01c454c2a146aaa557f7ef47506f92c2e6e4d80e','a4fef9e37acb5f47b20a5dfae50e2afe','2016-10-31 04:52:45','2016-10-31 03:52:45',0,1);

/*Table structure for table `transactions` */

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerFk` int(11) DEFAULT NULL,
  `bcodeFk` int(11) DEFAULT NULL,
  `leaseFk` int(11) DEFAULT NULL,
  `channelSubcategoryFk` int(11) DEFAULT NULL,
  `transactionRef` varchar(255) DEFAULT NULL,
  `transactionAmount` double DEFAULT NULL,
  `transactionDate` datetime DEFAULT NULL,
  `isUsed` tinyint(1) DEFAULT NULL,
  `dateUsed` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `transactions` */

insert  into `transactions`(`id`,`customerFk`,`bcodeFk`,`leaseFk`,`channelSubcategoryFk`,`transactionRef`,`transactionAmount`,`transactionDate`,`isUsed`,`dateUsed`) values (10,25,NULL,21,3,'trans3858jr8j4499',3000,'2016-10-26 00:00:00',1,'2016-10-26 12:46:18'),(11,27,NULL,23,3,'t33eg37g37e33e3e',3000,'2016-10-26 00:00:00',0,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
