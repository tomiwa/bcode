/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.6.17 : Database - bcode
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bcode` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bcode`;

/*Table structure for table `api_activity_logs` */

DROP TABLE IF EXISTS `api_activity_logs`;

CREATE TABLE `api_activity_logs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `endPoint` varchar(255) DEFAULT NULL,
  `request` varchar(255) DEFAULT NULL,
  `response` varchar(255) DEFAULT NULL,
  `activityDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=latin1;

/*Data for the table `api_activity_logs` */

insert  into `api_activity_logs`(`id`,`endPoint`,`request`,`response`,`activityDate`) values (77,NULL,'{\"request\":\"{\\n\\\"token\\\": \\\"28de0623ef384bb27b63f8fc68931222\\\",\\n\\\"lease\\\": \\\"0ddec2439fbc2afa82d0a2bb62d4948\\\",\\n\\\"transactionRef\\\": \\\"dhb3ubd3db3dvvhvhvhv\\\",\\n\\\"transactionRef\\\": \\\"dhb3ubd3db3dvvhvhvhv\\\",\\nS\\n\\n}\\n\\n\"}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:30:49'),(78,NULL,'{\"request\":null}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:31:42'),(79,'bcode/Api/v1/Service/verifyTransaction','{\"request\":null}','{\"response\":{\"code\":406,\"status\":\"false\",\"message\":\"Request body more or less than expected\"}}','2016-11-18 11:32:18'),(80,'bcode/Api/v1/Service/verifyTransaction','{\"request\":null}','{\"response\":{\"code\":406,\"status\":\"false\",\"message\":\"Request body more or less than expected\"}}','2016-11-18 11:32:41'),(81,NULL,'{\"request\":null}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:33:04'),(82,NULL,'{\"request\":null}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:33:34'),(83,NULL,'{\"request\":\"bcode\\/Api\\/v1\\/Service\\/verifyTransaction\"}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:36:24'),(84,'bcode/Api/v1/Service/auth','{\"request\":null}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:37:33'),(85,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"bus001\"}}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:37:54'),(86,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"bus001\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"token\":\"cbd76db242b96040e27c06d27717f71e\",\"message\":\"successfully authenticated.\"}}','2016-11-18 11:40:35'),(87,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"bus001\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"token\":\"cbd76db242b96040e27c06d27717f71e\",\"message\":\"Your session is still valid.\"}}','2016-11-18 11:41:08'),(88,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"bus001\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"token\":\"cbd76db242b96040e27c06d27717f71e\",\"message\":\"Your session is still valid.\"}}','2016-11-18 11:42:31'),(89,'bcode/Api/v1/Service/auth','{\"request\":null}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:42:40'),(90,'bcode/Api/v1/Service/auth','{\"request\":null}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:42:52'),(91,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"bus001\",\"channelSubcategoryCde\":\"bus001\"}}','{\"response\":{\"code\":406,\"status\":\"false\",\"message\":\"Request body more or less than expected\"}}','2016-11-18 11:43:24'),(92,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"bus001\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"token\":\"cbd76db242b96040e27c06d27717f71e\",\"message\":\"Your session is still valid.\"}}','2016-11-18 11:43:57'),(93,'bcode/Api/v1/Service/auth','{\"request\":null}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:45:32'),(94,'bcode/Api/v1/Service/auth','{\"request\":null}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:45:42'),(95,'bcode/Api/v1/Service/auth','{\"request\":\"<response>\\r\\n    <status\\/>\\r\\n        <!-- Error code (400, 403, 404, 500) -->\\r\\n    <message\\/>\\r\\n        <!-- Error description -->\\r\\n<\\/response>\"}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:48:14'),(96,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"bus001\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"token\":\"cbd76db242b96040e27c06d27717f71e\",\"message\":\"Your session is still valid.\"}}','2016-11-18 11:48:33'),(97,'bcode/Api/v1/Service/auth','{\"request\":\"{\\r\\n  \\\"username\\\": \\\"bus\\\",\\r\\n  \\\"password\\\": \\\"bus\\\",\\r\\n  \\\"channelSubcategoryCode\\\": \\\"bus001\\\",\\r\\n  \\\"channelSubcategoryCode\\\": \\\"bus001\\\",\\r\\n}\"}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 11:48:53'),(98,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"bus001\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"token\":\"cbd76db242b96040e27c06d27717f71e\",\"message\":\"Your session is still valid.\"}}','2016-11-18 11:49:20'),(99,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"bus001c\"}}','{\"response\":{\"code\":404,\"status\":\"false\",\"message\":\"Username, password or channelSubcategoryCode does not exist\"}}','2016-11-18 11:51:18'),(100,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"bus001c\"}}','{\"response\":{\"code\":404,\"status\":\"false\",\"message\":\"Username, password or channelSubcategoryCode does not exist\"}}','2016-11-18 12:23:54'),(101,'bcode/Api/v1/Service/auth','{\"request\":\"{\\r\\n  \\\"username\\\": \\\"bus\\\",\\r\\n  \\\"password\\\": \\\"bus\\\",\\r\\n  \\\"channelSubcategoryCode\\\": \\\"bus001\\\",\\r\\ns}\"}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Json Request Body Expected\"}}','2016-11-18 12:24:04'),(102,'bcode/Api/v1/Service/auth','{\"request\":\"{\\r\\n  \\\"username\\\": \\\"bus\\\",\\r\\n  \\\"password\\\": \\\"bus\\\",\\r\\n  \\\"channelSubcategoryCode\\\": \\\"bus001\\\",\\r\\ns}\"}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"invalid Json request body\"}}','2016-11-18 12:25:13'),(103,'bcode/Api/v1/Service/verifyTransaction','{\"request\":\"{\\n\\\"token\\\": \\\"28de0623ef384bb27b63f8fc68931222\\\",\\n\\\"lease\\\": \\\"0ddec2439fbc2afa82d0a2bb62d4948\\\",\\n\\\"transactionRef\\\": \\\"dhb3ubd3db3dvvhvhvhv\\\",\\n\\\"transactionRef\\\": \\\"dhb3ubd3db3dvvhvhvhv\\\",\\nS\\n\\n}\\n\\n\"}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"invalid Json request body\"}}','2016-11-18 12:25:23'),(104,'bcode/Api/v1/Service/verifyTransaction','{\"request\":{\"token\":\"28de0623ef384bb27b63f8fc68931222\",\"lease\":\"0ddec2439fbc2afa82d0a2bb62d4948\",\"transactionRef\":\"dhb3ubd3db3dvvhvhvhv\"}}','{\"response\":{\"code\":408,\"status\":\"false\",\"message\":\"session token expired, kindly re-authenticate\"}}','2016-11-18 12:25:38'),(105,'bcode/Api/v1/Service/verifyTransaction','{\"request\":{\"token\":\"28de0623ef384bb27b63f8fc68931222\",\"lease\":\"0ddec2439fbc2afa82d0a2bb62d4948\",\"transactionRef\":\"dhb3ubd3db3dvvhvhvhv\",\"transactionRf\":\"dhb3ubd3db3dvvhvhvhv\"}}','{\"response\":{\"code\":406,\"status\":\"false\",\"message\":\"Request body more or less than expected\"}}','2016-11-18 12:29:18'),(106,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"5225591b79f9b14bbb27025a17653b71\"}}','{\"response\":{\"code\":408,\"status\":\"false\",\"message\":\"session token expired, kindly re-authenticate\"}}','2016-11-18 16:04:14'),(107,'bcode/Api/v1/Service/auth','{\"request\":\"{\\r\\n  \\\"username\\\": \\\"bus\\\",\\r\\n  \\\"password\\\": \\\"bus\\\",\\r\\n  \\\"channelSubcategoryCode\\\": \\\"bus001\\\",\\r\\ns}\"}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"invalid Json request body\"}}','2016-11-18 16:05:29'),(108,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"bus001\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"token\":\"c3b3365c081ed7f573d9fa32bc46f355\",\"message\":\"successfully authenticated.\"}}','2016-11-18 16:06:29'),(109,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"c3b3365c081ed7f573d9fa32bc46f355\"}}','{\"response\":{\"code\":401,\"status\":\"false\",\"message\":\"You dont have permission to perform this operation\"}}','2016-11-18 16:06:59'),(110,'{\n   \"token\": \"c3b3365c081ed7f573d9fa32bc46f355\"a\n}','{\"request\":\"{\\n   \\\"token\\\": \\\"c3b3365c081ed7f573d9fa32bc46f355\\\"a\\n}\"}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"invalid Json request body\"}}','2016-11-18 16:07:45'),(111,'{\n   \"token\": \"c3b3365c081ed7f573d9fa32bc46f355\",\n}','{\"request\":\"{\\n   \\\"token\\\": \\\"c3b3365c081ed7f573d9fa32bc46f355\\\",\\n}\"}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"invalid Json request body\"}}','2016-11-18 16:08:18'),(112,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"tom\",\"password\":\"tom\",\"channelSubcategoryCode\":\"admin001\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\",\"message\":\"successfully authenticated.\"}}','2016-11-18 16:09:16'),(113,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}','{\"response\":\"{\\\"product_sub_categories\\\":[{\\\"id\\\":\\\"1\\\",\\\"channelCategoryFk\\\":\\\"1\\\",\\\"name\\\":\\\"All\\\",\\\"channelSubcategoryCode\\\":\\\"admin001\\\"},{\\\"id\\\":\\\"2\\\",\\\"channelCategoryFk\\\":\\\"2\\\",\\\"name\\\":\\\"efex\\\",\\\"channelSubcategoryCode\\\":\\\"bus001\\\"},{\\\"id\\\":\\\"3\\\",','2016-11-18 16:09:28'),(114,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}',NULL,'2016-11-18 16:11:39'),(115,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}',NULL,'2016-11-18 16:12:46'),(116,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}',NULL,'2016-11-18 16:14:01'),(117,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}','{\"response\":{\"product_sub_categories\":{\"id\":\"4\",\"channelCategoryFk\":\"3\",\"name\":\"phedc\",\"channelSubcategoryCode\":\"edc001\"}}}','2016-11-18 16:14:23'),(118,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}','{\"response\":{\"product_sub_categories\":[{\"id\":\"1\",\"channelCategoryFk\":\"1\",\"name\":\"All\",\"channelSubcategoryCode\":\"admin001\"},{\"id\":\"2\",\"channelCategoryFk\":\"2\",\"name\":\"efex\",\"channelSubcategoryCode\":\"bus001\"},{\"id\":\"3\",\"channelCategoryFk\":\"2\",\"name\":\"libra\",','2016-11-18 16:14:54'),(119,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}','{\"response\":[{\"id\":\"1\",\"channelCategoryFk\":\"1\",\"name\":\"All\",\"channelSubcategoryCode\":\"admin001\"},{\"id\":\"2\",\"channelCategoryFk\":\"2\",\"name\":\"efex\",\"channelSubcategoryCode\":\"bus001\"},{\"id\":\"3\",\"channelCategoryFk\":\"2\",\"name\":\"libra\",\"channelSubcategoryCode\":\"','2016-11-18 16:16:17'),(120,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}','{\"response\":{\"product_sub_categories\":[{\"id\":\"1\",\"channelCategoryFk\":\"1\",\"name\":\"All\",\"channelSubcategoryCode\":\"admin001\"},{\"id\":\"2\",\"channelCategoryFk\":\"2\",\"name\":\"efex\",\"channelSubcategoryCode\":\"bus001\"},{\"id\":\"3\",\"channelCategoryFk\":\"2\",\"name\":\"libra\",','2016-11-18 16:18:16'),(121,'bcode/Api/v1/Service/getAllproductCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}','{\"response\":\"{\\\"product_categories\\\":[{\\\"id\\\":\\\"1\\\",\\\"name\\\":\\\"all\\\"},{\\\"id\\\":\\\"2\\\",\\\"name\\\":\\\"bus\\\"},{\\\"id\\\":\\\"3\\\",\\\"name\\\":\\\"edc\\\"}]}\"}','2016-11-18 16:21:12'),(122,'bcode/Api/v1/Service/getAllproductCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}','{\"response\":{\"product_categories\":[{\"id\":\"1\",\"name\":\"all\"},{\"id\":\"2\",\"name\":\"bus\"},{\"id\":\"3\",\"name\":\"edc\"}]}}','2016-11-18 16:21:50'),(123,'bcode/Api/v1/Service/bcode','{\"request\":{\"token\":\"7d8a7d802dc2851a3c09f6165fa4a3b7\",\"channelSubcategoryCode\":\"bus001\",\"customerNumber\":\"07004914141\",\"customerName\":\"tope joke\",\"message\":\"message from bus001\",\"transactionRef\":\"dhb3ubd3db3dvvhvhvhv\",\"transactionAmount\":\"7000\"}}','{\"response\":{\"code\":408,\"status\":\"false\",\"message\":\"session token expired, kindly re-authenticate\"}}','2016-11-18 17:23:24'),(124,'{\n   \"token\": \"7d8a7d802dc2851a3c09f6165fa4a3b7\",\n   \"channelSubcategoryCode\": \"bus001\",\n   \"customerNumber\": \"07004914141\",\n   \"customerName\": \"tope joke\",\n   \"message\": \"message from bus001\",\n   \"transactionRef\": \"dhb3ubd3db3dvvhvhvhv\",\n   \"transactionA','{\"request\":\"{\\n   \\\"token\\\": \\\"7d8a7d802dc2851a3c09f6165fa4a3b7\\\",\\n   \\\"channelSubcategoryCode\\\": \\\"bus001\\\",\\n   \\\"customerNumber\\\": \\\"07004914141\\\",\\n   \\\"customerName\\\": \\\"tope joke\\\",\\n   \\\"message\\\": \\\"message from bus001\\\",\\n   \\\"transactionRef\\\": ','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"invalid Json request body\"}}','2016-11-18 17:23:43'),(125,'bcode/Api/v1/Service/bcode','{\"request\":{\"token\":\"7d8a7d802dc2851a3c09f6165fa4a3b7\",\"channelSubcategoryCode\":\"bus001\",\"customerNumber\":\"07004914141\",\"customerName\":\"tope joke\",\"message\":\"message from bus001\",\"transactionRef\":\"dhb3ubd3db3dvvhvhvhv\",\"transactionAmount\":\"7000\"}}','{\"response\":{\"code\":408,\"status\":\"false\",\"message\":\"session token expired, kindly re-authenticate\"}}','2016-11-18 18:42:22'),(126,'bcode/Api/v1/Service/bcode','{\"request\":{\"token\":\"7d8a7d802dc2851a3c09f6165fa4da3b7\",\"channelSubcategoryCode\":\"bus001\",\"customerNumber\":\"07004914141\",\"customerName\":\"tope joke\",\"message\":\"message from bus001\",\"transactionRef\":\"dhb3ubd3db3dvvhvhvhv\",\"transactionAmount\":\"7000\"}}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Invalid Token Supplied\"}}','2016-11-18 18:42:34'),(127,'bcode/Api/v1/Service/bcode','{\"request\":{\"token\":\"7d8a7d802dc2851a3c09f6165fa4a3b7\",\"channelSubcategoryCode\":\"bus001\",\"customerNumber\":\"07004914141\",\"customerName\":\"tope joke\",\"message\":\"message from bus001\",\"transactionRef\":\"dhb3ubd3db3dvvhvhvhv\",\"transactionAmount\":\"7000\"}}','{\"response\":{\"code\":408,\"status\":\"false\",\"message\":\"session token expired, kindly re-authenticate\"}}','2016-11-18 18:42:45'),(128,'bcode/Api/v1/Service/getAllproductCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}','{\"response\":{\"code\":408,\"status\":\"false\",\"message\":\"session token expired, kindly re-authenticate\"}}','2016-11-18 18:42:53'),(129,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}','{\"response\":{\"code\":408,\"status\":\"false\",\"message\":\"session token expired, kindly re-authenticate\"}}','2016-11-18 18:43:07'),(130,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a7x1b\"}}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"Invalid Token Supplied\"}}','2016-11-18 18:43:14'),(131,'bcode/Api/v1/Service/getAllproductSubCategory','{\"request\":{\"token\":\"07618d2058a826d6b2ab0be61bd0a71b\"}}','{\"response\":{\"code\":408,\"status\":\"false\",\"message\":\"session token expired, kindly re-authenticate\"}}','2016-11-18 18:43:20'),(132,'bcode/Api/v1/Service/verifyTransaction','{\"request\":{\"token\":\"7d8a7d802dc2851a3c09f6165fa4a3b7\",\"lease\":\"0ddec2439fbc2afa82d0a2bb62d4948\",\"transactionRef\":\"dhb3ubd3db3dvvhvhvhv\"}}','{\"response\":{\"code\":408,\"status\":\"false\",\"message\":\"session token expired, kindly re-authenticate\"}}','2016-11-18 18:43:48'),(133,'bcode/Api/v1/Service/bcode','{\"request\":{\"token\":\"7d8a7d802dc2851a3c09f6165fa4a3b7\",\"channelSubcategoryCode\":\"bus001\",\"customerNumber\":\"07004914141\",\"customerName\":\"tope joke\",\"message\":\"message from bus001\",\"transactionRef\":\"dhb3ubd3db3dvvhvhvhv\",\"transactionAmount\":\"7000\"}}','{\"response\":{\"code\":408,\"status\":\"false\",\"message\":\"session token expired, kindly re-authenticate\"}}','2016-11-18 19:14:16'),(134,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"edc\",\"password\":\"edc\",\"channelSubcategoryCode\":\"edc001\"}}','{\"response\":{\"code\":404,\"status\":\"false\",\"message\":\"Username, password or channelSubcategoryCode does not exist\"}}','2016-11-18 19:15:16'),(135,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"phedc\",\"password\":\"phedc\",\"channelSubcategoryCode\":\"edc001\"}}','{\"response\":{\"code\":404,\"status\":\"false\",\"message\":\"Username, password or channelSubcategoryCode does not exist\"}}','2016-11-18 19:16:09'),(136,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"edc\",\"password\":\"edc\",\"channelSubcategoryCode\":\"edc001\"}}','{\"response\":{\"code\":404,\"status\":\"false\",\"message\":\"Username, password or channelSubcategoryCode does not exist\"}}','2016-11-18 19:18:06'),(137,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"edc\",\"password\":\"edc\",\"channelSubcategoryCode\":\"edc002\"}}','{\"response\":{\"code\":404,\"status\":\"false\",\"message\":\"Username, password or channelSubcategoryCode does not exist\"}}','2016-11-18 19:18:22'),(138,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"edc\",\"password\":\"edc\",\"channelSubcategoryCode\":\"edc001\"}}','{\"response\":{\"code\":404,\"status\":\"false\",\"message\":\"Username, password or channelSubcategoryCode does not exist\"}}','2016-11-18 19:18:32'),(139,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"edc001\"}}','{\"response\":{\"code\":404,\"status\":\"false\",\"message\":\"Username, password or channelSubcategoryCode does not exist\"}}','2016-11-18 19:19:17'),(140,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"bus\",\"password\":\"bus\",\"channelSubcategoryCode\":\"bus001\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"token\":\"fb9e4222eaee8889f983bd88ee865ae3\",\"message\":\"successfully authenticated.\"}}','2016-11-18 19:19:25'),(141,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"edc\",\"password\":\"edc\",\"channelSubcategoryCode\":\"bus001\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"token\":\"cd291bbcdf5e07c5574e47f7b9f87eed\",\"message\":\"successfully authenticated.\"}}','2016-11-18 19:19:42'),(142,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"edc\",\"password\":\"edc\",\"channelSubcategoryCode\":\"bus001\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"token\":\"cd291bbcdf5e07c5574e47f7b9f87eed\",\"message\":\"Your session is still valid.\"}}','2016-11-18 19:21:41'),(143,'bcode/Api/v1/Service/auth','{\"request\":{\"username\":\"edc\",\"password\":\"edc\",\"channelSubcategoryCode\":\"bus002\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"token\":\"cd291bbcdf5e07c5574e47f7b9f87eed\",\"message\":\"Your session is still valid.\"}}','2016-11-18 19:22:02'),(144,'bcode/Api/v1/Service/bcode','{\"request\":{\"token\":\"cd291bbcdf5e07c5574e47f7b9f87eed\",\"channelSubcategoryCode\":\"bus001\",\"customerNumber\":\"07004914141\",\"customerName\":\"tope joke\",\"message\":\"message from bus001\",\"transactionRef\":\"dhb3ubd3db3dvvhvhvhv\",\"transactionAmount\":\"7000\"}}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"cannot post same transaction twice\"}}','2016-11-18 19:25:51'),(145,'bcode/Api/v1/Service/bcode','{\"request\":{\"token\":\"cd291bbcdf5e07c5574e47f7b9f87eed\",\"channelSubcategoryCode\":\"bus001\",\"customerNumber\":\"07004914143\",\"customerName\":\"tope joke\",\"message\":\"message from bus001\",\"transactionRef\":\"dhb3ubd3db3dvvhvhvhv\",\"transactionAmount\":\"7000\"}}','{\"response\":{\"code\":403,\"status\":\"false\",\"message\":\"cannot post same transaction twice\"}}','2016-11-18 19:26:20'),(146,'bcode/Api/v1/Service/bcode','{\"request\":{\"token\":\"cd291bbcdf5e07c5574e47f7b9f87eed\",\"channelSubcategoryCode\":\"bus001\",\"customerNumber\":\"07004914143\",\"customerName\":\"chinoso mike\",\"message\":\"message from bus001\",\"transactionRef\":\"2bd82b28db2d\",\"transactionAmount\":\"4000\"}}','{\"response\":{\"code\":200,\"status\":\"true\",\"message\":\"bcode generated and sent to customer, new transaction record set for customer\",\"bcodeRef\":\"1393665653889331168\",\"transactionRef\":\"2bd82b28db2d\"}}','2016-11-18 19:26:43');

/*Table structure for table `auth_session` */

DROP TABLE IF EXISTS `auth_session`;

CREATE TABLE `auth_session` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `permissionsFk` int(11) DEFAULT NULL,
  `rawToken` varchar(255) DEFAULT NULL,
  `channelAccountsFk` int(11) DEFAULT NULL,
  `channelCategory` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_session` */

/*Table structure for table `bcode` */

DROP TABLE IF EXISTS `bcode`;

CREATE TABLE `bcode` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `channelAccountFk` tinyint(1) DEFAULT NULL,
  `customersFK` int(255) DEFAULT NULL,
  `bcodeRef` varchar(255) DEFAULT NULL,
  `generationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `bcode` */

insert  into `bcode`(`id`,`channelAccountFk`,`customersFK`,`bcodeRef`,`generationDate`) values (1,2,1,'1393665653885139798','2016-11-02 12:16:03'),(2,2,2,'1393665653885139799','2016-11-02 12:21:53'),(3,2,3,'1393665653885139802','2016-11-02 12:29:44'),(4,2,4,'1393665653885139806','2016-11-02 12:37:01'),(5,2,5,'1393665653885139807','2016-11-02 12:41:08'),(6,2,6,'1393665653887365011','2016-11-11 14:01:33'),(7,2,7,'1393665653887365016','2016-11-11 14:03:08'),(8,2,8,'1393665653887692256','2016-11-14 13:34:29'),(9,2,9,'1393665653887692450','2016-11-14 15:59:09'),(10,4,10,'1393665653889331168','2016-11-18 19:26:41');

/*Table structure for table `bcode_api_activity_logs` */

DROP TABLE IF EXISTS `bcode_api_activity_logs`;

CREATE TABLE `bcode_api_activity_logs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `endPoint` varchar(255) DEFAULT NULL,
  `request` varchar(255) DEFAULT NULL,
  `response` varchar(255) DEFAULT NULL,
  `activityDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `bcode_api_activity_logs` */

insert  into `bcode_api_activity_logs`(`id`,`endPoint`,`request`,`response`,`activityDate`) values (1,'https://a.service-api.com/api/bcode/1393665653889331168/lease','\r\n                <data>\r\n                <numberOfLeases>1</numberOfLeases>\r\n                </data>','\"<response><status>0<\\/status><leases><lease startTS=\\\"2016-10-01T00:00:00.0\\\" endTS=\\\"2017-01-01T00:00:00.0\\\" hmac=\\\"9bc1e00cba02f5addc7b87340e2da8cc\\\" \\/><\\/leases><\\/response>\"','2016-11-18 19:26:43'),(2,'https://a.service-api.com/api/issuer','\r\n        <data>\r\n            <type>leased</type>\r\n            <gatewaynumber>SharedUK</gatewaynumber>\r\n            <msisdn>+2347004914143</msisdn>\r\n            <message>message from bus001 for more info on the usage of your unique bcode visit http://www.','\"<response><status>0<\\/status><bcode ref=\\\"1393665653889331168\\\" \\/><sms ref=\\\"1393665653889446913\\\" \\/><subscriber ref=\\\"1393665653889446912\\\" \\/><\\/response>\"','2016-11-18 19:26:43');

/*Table structure for table `bcode_lease_log` */

DROP TABLE IF EXISTS `bcode_lease_log`;

CREATE TABLE `bcode_lease_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lease` varchar(255) DEFAULT NULL,
  `bcodeFk` int(11) DEFAULT NULL,
  `generationDate` datetime DEFAULT NULL,
  `isExpired` tinyint(1) DEFAULT '0',
  `leaseStartDate` datetime DEFAULT NULL,
  `leaseEndDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

/*Data for the table `bcode_lease_log` */

insert  into `bcode_lease_log`(`id`,`lease`,`bcodeFk`,`generationDate`,`isExpired`,`leaseStartDate`,`leaseEndDate`) values (1,'0f2572e8ab7afb640b0613154a2c04ff',1,'2016-11-02 12:16:03',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(2,'e5118e47cfca918646783341be68c05a',2,'2016-11-02 12:21:53',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(3,'14f9410a95a4ac421481ff3fac05985e',3,'2016-11-02 12:29:44',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(4,'4b89f4c67af5afe195e1600138064117',4,'2016-11-02 12:37:01',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(5,'9aea003de3a091142fbf9e0413a76045',5,'2016-11-02 12:41:08',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(6,'08ab896c6e29c8f8542bdbe7340971ac',6,'2016-11-11 14:01:33',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(7,'d97c6d3429e36ee700737aa0f9040a12',7,'2016-11-11 14:03:08',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(8,'81351fc68e8603a33bdd6cc9249aa2d2',8,'2016-11-14 13:34:29',0,'2016-10-01 00:00:00','2017-01-01 00:00:00'),(9,NULL,NULL,'2016-11-14 14:35:59',0,NULL,NULL),(10,'8b92b042081015885651a693496f8235',1,'2016-11-14 14:39:42',0,'2017-01-01 00:00:00','2017-04-01 00:00:00'),(11,'fe35bbf4ce9ac2d69c6d9e416b706ee3',1,'2016-11-14 14:53:29',0,'2017-04-01 00:00:00','2017-07-01 00:00:00'),(12,NULL,NULL,'2016-11-14 15:02:21',0,NULL,NULL),(13,'7c83d02d3f525b428968d5decd7e1626',5,'2016-11-14 15:10:41',0,'2017-01-01 00:00:00','2017-04-01 00:00:00'),(14,'906ad548b28a9c688cee42667d669130',9,'2016-11-14 15:59:09',0,'2016-10-01 00:00:00','2016-01-01 00:00:00'),(15,'37dg3',0,'2016-11-14 15:59:09',0,'2016-10-01 00:00:00','2016-01-01 00:00:00'),(16,'V39H39F93FH39H',0,'2016-11-14 15:59:09',0,'2016-10-01 00:00:00','2017-12-09 00:00:00'),(30,'0ddec2439fbc2afa82d0a2bb62d49487',9,'2016-11-15 17:48:33',0,'2017-04-01 00:00:00','2017-07-01 00:00:00'),(32,'9bc1e00cba02f5addc7b87340e2da8cc',10,'2016-11-18 19:26:41',0,'2016-10-01 00:00:00','2017-01-01 00:00:00');

/*Table structure for table `bcode_sms_log` */

DROP TABLE IF EXISTS `bcode_sms_log`;

CREATE TABLE `bcode_sms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sms` varchar(255) DEFAULT NULL,
  `smsRef` varchar(255) DEFAULT NULL,
  `bCodeFk` int(11) DEFAULT NULL,
  `generationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `bcode_sms_log` */

insert  into `bcode_sms_log`(`id`,`sms`,`smsRef`,`bCodeFk`,`generationDate`) values (1,'hey Tomjazz, here is your bCode','1393665653885226345',1,'2016-11-02 12:16:03'),(2,'hey Tomjazz, here is your bCode','1393665653885226448',2,'2016-11-02 12:21:53'),(3,'hey Tomjazz, here is your bCode','1393665653885226483',3,'2016-11-02 12:29:44'),(4,'hey Tomjazz, here is your bCode','1393665653885226619',4,'2016-11-02 12:37:01'),(5,'hey Tomjazz, here is your bCode','1393665653885226690',5,'2016-11-02 12:41:08'),(6,'message from bus002','1393665653887453963',1,'2016-11-11 13:51:01'),(7,'message from bus002 for more info on the usage of your unique bcode visit http://www.techadvance.ng','1393665653887454172',6,'2016-11-11 14:01:33'),(8,'message from bus002 for more info on the usage of your unique bcode visit http://www.techadvance.ng','1393665653887454185',7,'2016-11-11 14:03:08'),(9,'message from bus002','1393665653887454202',7,'2016-11-11 14:06:31'),(10,'message from bus002','1393665653887454437',1,'2016-11-11 14:13:16'),(11,'message from bus002','1393665653887868772',2,'2016-11-14 13:23:54'),(12,'message from bus001','1393665653887868815',2,'2016-11-14 13:29:06'),(13,'message from bus001','1393665653887868938',2,'2016-11-14 13:32:50'),(14,'message from bus001 for more info on the usage of your unique bcode visit http://www.techadvance.ng','1393665653887868950',8,'2016-11-14 13:34:29'),(15,'message from bus001 for more info on the usage of your unique bcode visit http://www.techadvance.ng','1393665653887872094',9,'2016-11-14 15:59:09'),(16,'message from bus001','1393665653887873377',9,'2016-11-14 16:32:57'),(17,'message from bus001','1393665653887873650',9,'2016-11-14 16:47:42'),(18,'message from bus001','1393665653887873653',9,'2016-11-14 16:48:55'),(19,'message from bus001','1393665653887873655',9,'2016-11-14 16:49:57'),(20,'message from bus001','1393665653887875036',9,'2016-11-14 17:38:02'),(21,'message from bus001','1393665653888643944',NULL,'2016-11-15 14:35:17'),(22,'message from bus001','1393665653888643982',NULL,'2016-11-15 14:39:31'),(23,'message from bus001','1393665653888645211',9,'2016-11-15 17:46:09'),(24,'message from bus001','1393665653888645458',9,'2016-11-15 18:32:00'),(25,'message from bus001 for more info on the usage of your unique bcode visit http://www.techadvance.ng','1393665653889446913',10,'2016-11-18 19:26:41');

/*Table structure for table `channel_accounts` */

DROP TABLE IF EXISTS `channel_accounts`;

CREATE TABLE `channel_accounts` (
  `id` int(250) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `permissionsFk` int(11) DEFAULT NULL,
  `channelCategoryFk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `channel_accounts` */

insert  into `channel_accounts`(`id`,`username`,`password`,`permissionsFk`,`channelCategoryFk`) values (1,'tom','tom',1,1),(2,'bus','bus',2,2),(3,'tunde','tunde',1,1),(4,'edc','edc',2,2);

/*Table structure for table `channel_category` */

DROP TABLE IF EXISTS `channel_category`;

CREATE TABLE `channel_category` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `channel_category` */

insert  into `channel_category`(`id`,`name`) values (1,'all'),(2,'bus'),(3,'edc');

/*Table structure for table `channel_sub_category` */

DROP TABLE IF EXISTS `channel_sub_category`;

CREATE TABLE `channel_sub_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channelCategoryFk` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `channelSubcategoryCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `channel_sub_category` */

insert  into `channel_sub_category`(`id`,`channelCategoryFk`,`name`,`channelSubcategoryCode`) values (1,1,'All','admin001'),(2,2,'efex','bus001'),(3,2,'libra','bus002'),(4,3,'phedc','edc001');

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `customerName` varchar(250) DEFAULT NULL,
  `phone` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `customers` */

insert  into `customers`(`id`,`customerName`,`phone`) values (1,'Tomjazz','+2349094625113'),(2,'Tomjazz','+2347064985162'),(3,'Tomjazz','+2347000000002'),(4,'Tomjazz','+2349000000002'),(5,'Tomjazz','+2342000000002'),(6,'Wale','+2347036650660'),(7,'Wale','+2347036650669'),(8,'Tomiwa chales','+2347064900000'),(9,'tope joke','+2347004914141'),(10,'chinoso mike','+2347004914143');

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissions` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`permissions`) values (1,'Admin'),(2,'User');

/*Table structure for table `subscriber_ref_log` */

DROP TABLE IF EXISTS `subscriber_ref_log`;

CREATE TABLE `subscriber_ref_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriberRef` varchar(255) DEFAULT NULL,
  `bCodeFk` int(11) DEFAULT NULL,
  `generationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `subscriber_ref_log` */

insert  into `subscriber_ref_log`(`id`,`subscriberRef`,`bCodeFk`,`generationDate`) values (1,'1393665653879296094',1,'2016-11-02 12:16:03'),(2,'1393665653879331325',2,'2016-11-02 12:21:53'),(3,'1393665653885226482',3,'2016-11-02 12:29:44'),(4,'1393665653885226618',4,'2016-11-02 12:37:01'),(5,'1393665653885226689',5,'2016-11-02 12:41:08'),(6,'1393665653879296094',1,'2016-11-11 13:51:01'),(7,'1393665653887453979',6,'2016-11-11 14:01:33'),(8,'1393665653887454184',7,'2016-11-11 14:03:08'),(9,'1393665653887454184',7,'2016-11-11 14:06:31'),(10,'1393665653879296094',1,'2016-11-11 14:13:16'),(11,'1393665653887868949',8,'2016-11-14 13:34:29'),(12,'1393665653887872093',9,'2016-11-14 15:59:09'),(13,'1393665653889446912',10,'2016-11-18 19:26:41');

/*Table structure for table `token` */

DROP TABLE IF EXISTS `token`;

CREATE TABLE `token` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `tokenString` varchar(255) DEFAULT NULL,
  `rawToken` varchar(255) DEFAULT NULL,
  `expiration` datetime DEFAULT NULL,
  `dateGenerated` datetime DEFAULT NULL,
  `isExpired` tinyint(1) DEFAULT NULL,
  `channelAccountsFk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `token` */

insert  into `token`(`id`,`tokenString`,`rawToken`,`expiration`,`dateGenerated`,`isExpired`,`channelAccountsFk`) values (1,'d86bd24836ad499ff041213d5df96527c0a9da91c614b2cf07472c207c04145b','22e183502291b14f743b6d40e979febb','2016-11-02 13:06:02','2016-11-02 12:06:02',0,2),(2,'2298caf2ca4dca337ac712b2fb1c1c8f38d2e0e11979f6c96e5eea956235b914','5225591b79f9b14bbb27025a17653b71','2016-11-02 13:46:57','2016-11-02 12:46:57',0,1),(3,'00d8aa573046625d5f0dc18fef645c21d35651fa49ad9f9ff047e8f6c106d1fe','b67adb270817c55f7ed20ca0b94a3ca8','2016-11-11 14:34:24','2016-11-11 13:34:24',0,2),(4,'320ab35efaf015bf638e64f8c8ab483be86f46aae144a4b3c57abc3959e4f893','86e5c319b3f530923b22df6438775823','2016-11-14 14:18:05','2016-11-14 13:18:05',0,2),(5,'721be845f815474cabb4171fddfceeaaa33852dfe8809680d39cf5c85024600c','2662e8624c331ac6d02ed39fc92d38bf','2016-11-14 15:29:55','2016-11-14 14:29:55',0,2),(6,'e86438678c6b595e7edb118ad4820a15fd5f05e7c8255cad70e27d180f279f2e','17ca2b78915f668f4327266f69a5483e','2016-11-14 16:46:12','2016-11-14 15:46:12',0,2),(7,'0a2bb3211ecc07c3fe01c266e83f20ce10674462ec304838a6cb191fdf9efe84','f622366dc1544d8f192b6b451bbc847f','2016-11-14 17:47:12','2016-11-14 16:47:12',0,2),(8,'7faedfc58adebff13396c4adb0a7e2edcd609fd91778ef90515a6096768e61a0','cff02b3418103ff94cc574319e0851c1','2016-11-15 15:34:53','2016-11-15 14:34:53',0,2),(9,'0a948d998244eb773f7366144eaf3e555c2a01b4177838473d349ee46820e92c','2f48f81bbcab91eab6b275e034446f90','2016-11-15 16:35:28','2016-11-15 15:35:28',0,2),(10,'d3af8b2c5afabffb67bbf8c7ea8f3ef7302944b61e09d1c2d2036de274e9afd1','ce0c2803ead276cf81d3a6cee1b757a2','2016-11-15 17:35:52','2016-11-15 16:35:52',0,2),(11,'e5b0eb02d74acf299438f93efffb5404be35d7c15ee6180b14967da29da7f9d4','7d8a7d802dc2851a3c09f6165fa4a3b7','2016-11-15 18:45:55','2016-11-15 17:45:55',0,2),(12,'77545be20993c700d1e326a5dd42c25687f14ca97994b97a8bfa4bba4e6e0d96','6bed5e7e94f3b8c36b64d6100bb285bf','2016-11-16 16:00:49','2016-11-16 15:00:49',0,2),(13,'bb7ee0d413a261c7c9f23d5273998ac27f9466cfbb50759fe9adf0aa499169c1','3b444d9b0d443ab5ca4637a359b829f5','2016-11-16 16:20:11','2016-11-16 15:20:11',0,1),(14,'e445dc5861ab3907a8b58caa0450ff4d54e3f1ff24ca6bdfb687e8a7b476cfa5','3224bd3f1420ca82b6df9264d2a7fa8a','2016-11-16 17:25:51','2016-11-16 16:25:51',0,1),(15,'10e3b41ff086fd0532b5df1bc129bcd1ff216ed3d5443c0874bf6a2be6ec31fb','244e99c5c454e9db20a61a6befb77533','2016-11-16 17:46:49','2016-11-16 16:46:49',0,2),(16,'55e156124391dbad31ab208adcc390a2380057ad5ba7712be5f9ce88cfffa671','50324341378a2f97fab151419cca8052','2016-11-17 14:22:51','2016-11-17 13:22:51',0,2),(17,'c2211e2de720fce171b1a214223047a90c6d1a866063d3ede1ec7a42c85a9c10','6b36900b15db0beb6cae7200d3341c21','2016-11-17 17:53:03','2016-11-17 16:53:03',0,2),(18,'e688355eded594ff88e5f270c9e18757b0d639977d4ed106529dabf2bfdcd987','28de0623ef384bb27b63f8fc68931222','2016-11-18 11:02:20','2016-11-18 10:02:20',0,2),(19,'1993e455a9775262ad9f40f7b3867d2648688feb7069f8c15fb3601645b6a97b','cbd76db242b96040e27c06d27717f71e','2016-11-18 12:40:34','2016-11-18 11:40:34',0,2),(20,'f420542e83d57fb4045048d6a08bb99262cb30aa43387a23496e5f1905f395ce','c3b3365c081ed7f573d9fa32bc46f355','2016-11-18 17:06:29','2016-11-18 16:06:29',0,2),(21,'3e8f8dde93a8179e01d6658687382ec0a86a386d76511537a6229bca982fdf9c','07618d2058a826d6b2ab0be61bd0a71b','2016-11-18 17:09:16','2016-11-18 16:09:16',0,1),(22,'2e36304d0dd9ec21b7cda37c5174e4359ce26f136992fae1e57390ac517726c1','fb9e4222eaee8889f983bd88ee865ae3','2016-11-18 20:19:25','2016-11-18 19:19:25',0,2),(23,'1af14d79e9da4d61af635d1546d913e476453ae7fbd790a9f0e693e766d1acc7','cd291bbcdf5e07c5574e47f7b9f87eed','2016-11-18 20:19:42','2016-11-18 19:19:42',0,4);

/*Table structure for table `transactions` */

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerFk` int(11) DEFAULT NULL,
  `bcodeFk` int(11) DEFAULT NULL,
  `leaseFk` int(11) DEFAULT NULL,
  `passingLeaseFk` int(11) DEFAULT NULL,
  `channelSubcategoryCode` varchar(255) DEFAULT NULL,
  `transactionRef` varchar(255) DEFAULT NULL,
  `transactionAmount` double DEFAULT NULL,
  `transactionDate` datetime DEFAULT NULL,
  `isUsed` tinyint(1) DEFAULT NULL,
  `dateUsed` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Data for the table `transactions` */

insert  into `transactions`(`id`,`customerFk`,`bcodeFk`,`leaseFk`,`passingLeaseFk`,`channelSubcategoryCode`,`transactionRef`,`transactionAmount`,`transactionDate`,`isUsed`,`dateUsed`) values (1,1,1,1,NULL,'bus002','trans666ff66f6382',3000,'2016-10-26 00:00:00',0,NULL),(2,2,NULL,2,NULL,'bus001','trans222222227ccc',3000,'2016-10-26 00:00:00',0,NULL),(3,2,2,2,0,'bus002','CCCC222222227ccc',3000,'2016-10-26 00:00:00',1,'2016-11-14 15:51:56'),(4,3,NULL,3,NULL,'bus002','tstatdte3heheheeejejej',3000,'2016-10-26 00:00:00',0,NULL),(5,4,NULL,4,NULL,'bus002','hshshshshshshhsh',3000,'2016-10-26 00:00:00',0,NULL),(6,5,5,5,7,'bus002','DLLLSLDLDLDLDLLD',3000,'2016-10-26 00:00:00',1,'2016-11-14 15:10:41'),(7,5,5,5,NULL,'bus002','dmsleygtuedi',3000,'2016-10-26 00:00:00',0,NULL),(8,1,1,1,NULL,'bus002','tRANS000000001',3000,'2016-11-11 13:51:01',0,NULL),(9,6,6,6,NULL,'bus002','transWales5577f',3000,'2016-11-11 14:01:33',0,NULL),(10,7,7,7,NULL,'bus002','Trans6g6f66ff6',3000,'2016-11-11 14:03:08',0,NULL),(11,7,7,7,NULL,'bus002','Trans6g6f66ff76',3000,'2016-11-11 14:06:31',0,NULL),(12,NULL,1,1,NULL,'bus002','Trans6g6f66f4fff76',3000,'2016-11-11 14:13:16',1,'2016-11-14 15:02:22'),(13,NULL,2,2,NULL,'bus001','tRANS0nndnf993fn1',4000,'2016-11-14 13:23:54',0,NULL),(14,NULL,2,2,NULL,'bus001','hf8f48f4bf8fb4f8',4000,'2016-11-14 13:29:06',0,NULL),(15,2,2,2,NULL,'bus001','hf8f48f4bf8fdjjdjdjb4f8',4000,'2016-11-14 13:32:50',0,NULL),(16,8,8,8,NULL,'bus001','hf8f48f4bf8fdjjdjddndddjdjsjb4f8',4000,'2016-11-14 13:34:29',0,NULL),(25,9,9,14,30,'bus001','dhb3ubd3db3d',7000,'2016-11-15 17:46:09',1,'2016-11-15 18:30:07'),(26,9,9,30,30,'bus001','dhb3ubd3db3dvvhvhvhv',7000,'2016-11-15 18:32:00',1,'2016-11-15 18:32:07'),(27,10,10,32,NULL,'bus001','2bd82b28db2d',4000,'2016-11-18 19:26:41',0,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
