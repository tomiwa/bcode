
<?php

include "__.php";
class Repo extends __
{

    public $db;


    public function __construct()
    {
        new __();

        try {
            $conn =  new PDO("mysql:host=".$this->dbh().";dbname=".$this->dbn(), $this->dbu(), $this->dbp());
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        $this->db = $conn;






    }







  /**  public function __construct(){

        //$this->mysqli = new mysqli($host, $username, $password, $db_name);
        $this->mysqli = new mysqli($this->dbh(), $this->dbu(),$this->dbp(),$this->dbn());



        if(mysqli_connect_errno()) {

            echo "Error: Could not connect to database.";

            exit;

        }
        else{
            echo"Your Database successfully connected";
            exit;
        }

    }

    public function __destruct(){
        $this->mysqli->close();
    }
**/



    public function getConnection()
    {
        return $this->db;
    }


    public function  userIsAvailable($username, $password, $channelSubcategoryCode)
    {
        $db = $this->getConnection();
        $hashedPwd = hash('sha256', $password);

        $result = $db->prepare("SELECT * FROM channel_accounts WHERE username = :username AND password =:password");
        $result->execute([
            'username' => $username,
            'password' => $hashedPwd
        ]);

        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                $channelCategoryFk = $rows['channelCategoryFk'];


            $result = $db->prepare("SELECT * FROM channel_sub_category WHERE channelSubcategoryCode = :channelSubcategoryCode");
            $result->execute([
                'channelSubcategoryCode' => $channelSubcategoryCode
            ]);

            if ($result->rowCount()) {
                while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                    $channelCatFk= $rows['channelCategoryFk'];


                if ($channelCategoryFk == $channelCatFk){
                    return true;

                }else{
                    return false;
                }

            }else{
                return false;
            }



        return true;

        } else {

            return false;
        }


    }






public function userIsInDatabase($username, $password, $channelCategoryFk){

    $db = $this->getConnection();
    $hashedPwd = hash('sha256', $password);

    $result = $db->prepare("SELECT * FROM channel_accounts WHERE username = :username AND password =:password AND channelCategoryFk=:channelCategoryFk");
    $result->execute([
        'username' => $username,
        'password' => $hashedPwd,
        'channelCategoryFk' => $channelCategoryFk
    ]);

    if ($result->rowCount()) {


        return true;

    } else {

        return false;
    }



}







    public function isUserTokenOpened($username, $password)
    {
        $db = $this->getConnection();
        $hashedPwd = hash('sha256', $password);

        $result = $db->prepare("SELECT * FROM channel_accounts WHERE username = :username AND password =:password");
        $result->execute([
            'username' => $username,
            'password' => $hashedPwd
        ]);

        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                $channelAccountsFk = $rows['id'];
            $nowDateTime = new DateTime('NOW');
            $nowDateTime = $nowDateTime->format('Y-m-d H:i:s');


            $result = $db->prepare("SELECT * FROM token WHERE channelAccountsFk = :channelAccountsFk AND expiration >=:nowDateTime");
            $result->execute([
                'channelAccountsFk' => $channelAccountsFk,
                'nowDateTime' => $nowDateTime
            ]);

            if ($result->rowCount()) {

                while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                    return $rows['rawToken'];
            } else {
                $token = bin2hex(openssl_random_pseudo_bytes(16));
                return $token;

            }
        }
    }


    public function isTokenExpired($tokenString)
    {
        $db = $this->getConnection();
        $nowDateTime = new DateTime('NOW');
        $nowDateTime = $nowDateTime->format('Y-m-d H:i:s');
        $hashedToken = hash('sha256', $tokenString);
        $result = $db->prepare("SELECT * FROM token WHERE tokenString = :hashedToken AND expiration <=:nowDateTime");
        $result->execute([
            'hashedToken' => $hashedToken,
            'nowDateTime' => $nowDateTime
        ]);

        if ($result->rowCount()) {
            return true;
        } else {
            return false;
        }

    }


    public function isUserRequestTokenValid($requestToken)
    {
        $db = $this->getConnection();

        $result = $db->prepare("SELECT * FROM token WHERE rawToken = :requestToken");
        $result->execute([
            'requestToken' => $requestToken
        ]);

        if ($result->rowCount()) {

            return true;
        } else {
            return false;
        }
    }







    /*
     public function destroySessionData($rawToken){

        $hashedToken = hash('sha256', $rawToken);
        $query = "UPDATE token SET isExpired=true WHERE  tokenString='$hashedToken'";
        $this->mysqli->query($query);
        return $this->deleteSession($rawToken);
    }*/


    /* public function deleteSession($rawToken){

         $query="DELETE FROM auth_session WHERE rawToken='$rawToken'";
         return $this->mysqli->query($query);// or die(mysqli_connect_errno()."Data cannot delete");
     }*/


    /*  public function insertSessionData($username,$password,$permissionsFk,$rawToken,$channelAccountsFk, $channelCategory)
      {

              $query = "INSERT INTO auth_session SET username='$username', password='$password', permissionsFk='$permissionsFk', rawToken ='$rawToken', channelAccountsFk='$channelAccountsFk',channelCategory='$channelCategory'";
              return $this->mysqli->query($query);



    public function sessionChannelAccountsId($username, $password)
    {

        $db = $this->getConnection();

            $query ="SELECT * FROM channel_accounts WHERE  username='$username'AND  password='$password'";
        $result= $this->mysqli->query($query);
        if($result->num_rows > 0) {
            while ($rows = mysqli_fetch_assoc($result)){
               return $rows['id'];

            }
        }

    }


    public function sessionChannelAccountsPermissionsId($username, $password){

        $query ="SELECT * FROM channel_accounts WHERE  username='$username'AND  password='$password'";
        $result= $this->mysqli->query($query);
        if($result->num_rows > 0) {
            while ($rows = mysqli_fetch_assoc($result)){
                return $rows['permissionsFk'];
            }
        }

    }



    public function sessionChannelAccountProductCatId($username, $password)
    {

        $query = "SELECT * FROM channel_accounts WHERE  username='$username'AND  password='$password'";
        $result = $this->mysqli->query($query);
        if ($result->num_rows > 0)
            while ($rows = mysqli_fetch_assoc($result))
                $channelCatId = $rows['channelCategoryFk'];
                $query = "SELECT * FROM channel_category WHERE  id='$channelCatId'";
                $result = $this->mysqli->query($query);

                if ($result->num_rows >0)
                    while ($rows = mysqli_fetch_assoc($result))
                    return $rows['category'];


    }


**/


    public function isUserAdmin($token)
    {
        //$serviceObj = new Service();
        //$genericBusinessLogicObj = new GenericBusinessLogic();
        $db = $this->getConnection();
        $hashedToken = hash('sha256', $token);


        $result = $db->prepare("SELECT * FROM token WHERE tokenString = :hashedToken");
        $result->execute([
            'hashedToken' => $hashedToken
        ]);


        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                $channelAccountsFk = $rows['channelAccountsFk'];


            $result = $db->prepare("SELECT * FROM channel_accounts WHERE id = :channelAccountsFk");
            $result->execute([
                'channelAccountsFk' => $channelAccountsFk
            ]);

            if ($result->rowCount()) {

                while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                    $permissionsFk = $rows['permissionsFk'];


                $result = $db->prepare("SELECT * FROM permissions WHERE id = :permissionsFk");
                $result->execute([
                    'permissionsFk' => $permissionsFk
                ]);

                if ($result->rowCount()) {
                    while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                        if ($rows['permissions'] == 'Admin') {

                            return true;

                        } else {

                            return false;


                        }

                }

            }

        }


    }


    public function isChannelAccountCreated($username, $password, $permissionsFk, $channelCategoryFk)
    {
        $db = $this->getConnection();
        $hashedPwd = hash('sha256', $password);
        $result = $db->prepare("INSERT INTO channel_accounts SET username= :username, password= :password , permissionsFk= :permissionsFk, channelCategoryFk= :channelCategoryFk");
        $result->execute([
            'username' => $username,
            'password' => $hashedPwd,
            'permissionsFk' => $permissionsFk,
            'channelCategoryFk' => $channelCategoryFk
        ]);

        if ($result) {
            return true;
        } else {
            return false;
        }

    }


    public function getIdOfLoggedOnUser($username, $password)
    {
        $db = $this->getConnection();
        $hashedPwd = hash('sha256', $password);
        $result = $db->prepare("SELECT * FROM channel_accounts WHERE username = :username AND password =:password");
        $result->execute([
            'username' => $username,
            'password' => $hashedPwd
        ]);

        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {

                return $rows['id'];
            }
        }

    }


    public function insertNewToken($rawToken, $channelAccountsFk)
    {
        $serviceObj = new Service();
        $confObj = new __();
        $created = new DateTime('NOW');
        $created = $created->format('Y-m-d H:i:s');
        $expires = new DateTime('NOW');
        $expires->add(new DateInterval($confObj->tokenExpirationTime()));
        $expires = $expires->format('Y-m-d H:i:s');
        $hashedToken = hash('sha256', $rawToken);


        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM token WHERE tokenString = :hashedToken");
        $result->execute([
            'hashedToken' => $hashedToken
        ]);


        if ($result->rowCount() <= 0) {


            $result = $db->prepare("INSERT INTO token SET tokenString=:hashedToken, rawToken= :rawToken, dateGenerated= :created, expiration= :expires, isExpired =:bool, channelAccountsFk = :channelAccountsFk");
            $result->execute([
                'hashedToken' => $hashedToken,
                'rawToken' => $rawToken,
                'created' => $created,
                'expires' => $expires,
                'bool' => false,
                'channelAccountsFk' => $channelAccountsFk
            ]);

            if ($result) {
                return true;
            }
        } else {

            return false;
        }

    }


    public function isproductCategoryAlreadyCreated($CatName)
    {
        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM channel_category WHERE name = :CatName");
        $result->execute([
            'CatName' => $CatName
        ]);

        if ($result->rowCount()) {
            return true;
        } else {

            return false;
        }


    }


    public function createProductCategory($CatName)
    {

        $db = $this->getConnection();
        $result = $db->prepare("INSERT INTO channel_category SET name=:CatName");
        $result->execute([
            'CatName' => $CatName,
        ]);

        if ($result) {
            return true;

        } else {
            return false;
        }

    }


    public function returnChannelProductCategoryName($CatName)
    {

        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM channel_category WHERE name = :CatName");
        $result->execute([
            'CatName' => $CatName
        ]);

        if ($result->rowCount()) {
            while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                $name = $rows['name'];
            return $name;
        }

    }


    public function returnChannelProductCategoryId($CatName)
    {

        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM channel_category WHERE name = :CatName");
        $result->execute([
            'CatName' => $CatName
        ]);

        if ($result->rowCount()) {
            while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                $id = $rows['id'];
            return $id;
        }

    }


    public function returnAllProductCategory()
    {
        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM channel_category");
        $result->execute();

        if ($result->rowCount()) {

        for($rows = array(); $row = $result->fetch(PDO::FETCH_ASSOC); $rows[] = $row);

            $arr = array('product_categories' =>$rows);
           // $json = json_encode($arr);

            return $arr ;


        }else{
            return false;
        }

    }





    public function returnAllProductSubCategory()
    {
        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM channel_sub_category");
        $result->execute();

        if ($result->rowCount()) {

            for($rows = array(); $row = $result->fetch(PDO::FETCH_ASSOC); $rows[] = $row);

            $arr = array('product_sub_categories' =>$rows);
            //$json = json_encode($arr);

            return $arr ;


        }else{
            return false;
        }

    }






    public function isproductSubCategoryAlreadyCreated($channelSubCategoryName, $channelCategoryFk, $channelSubcategoryCode)
    {
        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM channel_sub_category WHERE name =:channelSubCategoryName OR channelCategoryFk=:channelCategoryFk OR channelSubcategoryCode=:channelSubcategoryCode ");
        $result->execute([
            'channelSubCategoryName' => $channelSubCategoryName,
            'channelCategoryFk' => $channelCategoryFk,
            'channelSubcategoryCode' => $channelSubcategoryCode
        ]);

        if ($result->rowCount()) {
            return true;
        }else{
            return false;
        }




    }








    public function createProductSubCategory($channelSubCategoryName, $channelCategoryFk, $channelSubcategoryCode)
    {

        $db = $this->getConnection();
        $result = $db->prepare("INSERT INTO channel_sub_category SET name=:channelSubCategoryName, channelCategoryFk= :channelCategoryFk, channelSubcategoryCode=:channelSubcategoryCode");
        $result->execute([
            'channelSubCategoryName' => $channelSubCategoryName,
            'channelCategoryFk' => $channelCategoryFk,
            'channelSubcategoryCode' =>$channelSubcategoryCode
        ]);

        if ($result) {
            return true;

        } else {
            return false;
        }

    }


    public function returnProductCategoryName($channelCategoryFk)
    {

        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM channel_category WHERE id = :channelCategoryFk");
        $result->execute([
            'channelCategoryFk' => $channelCategoryFk
        ]);

        if ($result->rowCount()) {
            while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                $name = $rows['name'];
            return $name;
        }

    }


    public function returnProductCategoryId($channelCategoryFk)
    {

        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM channel_category WHERE id = :channelCategoryFk");
        $result->execute([
            'channelCategoryFk' => $channelCategoryFk
        ]);

        if ($result->rowCount()) {
            while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                $id = $rows['id'];
            return $id;
        }else{
            return false;
        }

    }


    public function returnProductChannelCategoryId($rawToken)
    {

        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM token WHERE rawToken = :rawToken");
        $result->execute([
            'rawToken' => $rawToken
        ]);

        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {
                $channelAccountsId = $rows['channelAccountsFk'];

                $result = $db->prepare("SELECT * FROM channel_accounts WHERE id = :channelAccountsId");
                $result->execute([
                    'channelAccountsId' => $channelAccountsId
                ]);


                if ($result->rowCount()) {
                    while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                        $channelCategoryFk = $rows['channelCategoryFk'];


                    return $channelCategoryFk;
                }
            }


        }

    }









    public function returnChannelAccountsFk($rawToken)
    {

        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM token WHERE rawToken = :rawToken");
        $result->execute([
            'rawToken' => $rawToken
        ]);

        if ($result->rowCount())
            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {
                $channelAccountsId = $rows['channelAccountsFk'];

                return $channelAccountsId;


            }

    }


    public function returnCustomersFk($customerNumber)
    {
        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM customers WHERE phone=:customerNumber ");

        $result->execute([
            'customerNumber' => $customerNumber
        ]);


        if ($result->rowCount())
            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {
                $customerId = $rows['id'];
                return $customerId;
            }

    }


    public function returnbCodeFk($bCodeRef)
    {

        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM bcode WHERE bcodeRef=:bcodeRef");

        $result->execute([
            'bcodeRef' => $bCodeRef
        ]);


        if ($result->rowCount())
            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {
                $bcodeId = $rows['id'];
                return $bcodeId;
            }

    }


    public function returnLeaseFk($bCodeRef)
    {


        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM bcode WHERE bcodeRef=:bcodeRef");

        $result->execute([
            'bcodeRef' => $bCodeRef
        ]);


        if ($result->rowCount())
            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {
                $bcodeId = $rows['id'];

                //check if there is any valid lease for customer and if none exist for customer, issue a new lease

                $result =$this->getCustomerLease($bcodeId,$bCodeRef);
                 $lease = $result[0];
                 $leaseId = $result[1];


              /*  $nowDateTime = new DateTime('NOW');
                $nowDateTime = $nowDateTime->format('Y-m-d');
                $result = $db->prepare("SELECT * FROM bcode_lease_log WHERE bcodeFk=:bcodeId AND leaseEndDate >:nowDateTime");

                $result->execute([
                    'bcodeId' => $bcodeId,
                    'nowDateTime' =>$nowDateTime
                ]);

                if ($result->rowCount())
                    while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                        $leaseid = $rows['id'];*/

                return $leaseId;


            }

    }











   /** public function returnAlmostExpiringLeaseFk($bCodeRef)
    {

        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM bcode WHERE bcodeRef=:bcodeRef");

        $result->execute([
            'bcodeRef' => $bCodeRef
        ]);



        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {
                $bcodeId = $rows['id'];



              //  $oneWeekFromNowDate = date('Y-m-d', strtotime('+1 week'));

                $nowDate = new DateTime('NOW');
                $nowDate = $nowDate->format('Y-m-d');

                $result = $db->prepare("SELECT * FROM bcode_lease_log WHERE bcodeFk=:bcodeId ORDER BY leaseEndDate ASC");

                $result->execute([
                    'bcodeId' => $bcodeId
                   // 'oneWeekFromNowDate' =>$oneWeekFromNowDate
                ]);


                if ($result->rowCount()) {
                    while ($rows = $result->fetch(PDO::FETCH_ASSOC))



                        foreach ($result as $rows)

                        if ($rows['leaseEndDate'] >= $nowDate )
                            print_r( );
                            exit();

                            $leaseid = $rows['id'];


                            // return $leaseid;



                }
            }

        }

    }**/







    public function insertCustomer($name, $phone)
    {


        $db = $this->getConnection();
        $result = $db->prepare("INSERT INTO customers SET customerName=:customerName, phone=:phone");
        $result->execute([
            'customerName' => $name,
            'phone' => $phone
        ]);

        if ($result) {
            return true;

        } else {
            return false;
        }


    }


    public function inserLease($lease, $bcodeFk, $generationDate, $leaseStartDate, $leaseEndDate)
    {
        $db = $this->getConnection();
        $result = $db->prepare("INSERT INTO bcode_lease_log SET lease=:lease,bcodeFk=:bcodeFk,generationDate=:generationDate,leaseStartDate=:leaseStartDate,leaseEndDate=:leaseEndDate");
        $result->execute([
            'lease' => $lease,
            'bcodeFk' => $bcodeFk,
            'generationDate' => $generationDate,
            'leaseStartDate' => $leaseStartDate,
            'leaseEndDate' => $leaseEndDate
        ]);


    }


    public function insertbCode($channelAccountFk, $customersFk, $bcodeRef, $generationDate)
    {

        $db = $this->getConnection();
        $result = $db->prepare("INSERT INTO bcode SET channelAccountFk=:channelAccountFk, customersFk=:customersFk, bcodeRef=:bcodeRef, generationDate=:generationDate");
        $result->execute([
            'channelAccountFk' => $channelAccountFk,
            'customersFk' => $customersFk,
            'bcodeRef' => $bcodeRef,
            'generationDate' => $generationDate
        ]);


    }


    public function insertSubscriberRef($subscriberRef, $bcodeFk, $generationDate)
    {

        $db = $this->getConnection();
        $result = $db->prepare("INSERT INTO subscriber_ref_log SET subscriberRef=:subscriberRef, bCodeFk=:bcodeFk, generationDate=:generationDate");
        $result->execute([
            'subscriberRef' => $subscriberRef,
            'bcodeFk' => $bcodeFk,
            'generationDate' => $generationDate
        ]);
    }


    public function insertSmsRef($message, $smsRef, $bcodeFk, $generationDate)
    {

        $db = $this->getConnection();
        $result = $db->prepare("INSERT INTO bcode_sms_log SET sms=:message,smsRef=:smsRef, bCodeFk=:bcodeFk, generationDate=:generationDate");
        $result->execute([
            'message' => $message,
            'smsRef' => $smsRef,
            'bcodeFk' => $bcodeFk,
            'generationDate' => $generationDate
        ]);

    }


    public function getCustomerbcodeRef($customerPhone)
    {



        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM customers WHERE phone=:customerPhone");
        $result->execute([
            'customerPhone' => $customerPhone
        ]);

        if ($result->rowCount())
            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {
                $customerId = $rows['id'];

                $result = $db->prepare("SELECT * FROM bcode WHERE customersFK=:customerId");
                $result->execute([
                    'customerId' => $customerId
                ]);

                if ($result->rowCount())
                    while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {

                        $bcodeId = $rows['id'];
                        $bcodeRef = $rows['bcodeRef'];
                        $arr =array($bcodeId,$bcodeRef);


                        return $arr;

                    } else {
                    return false;
                }


            }
    }


    public function isCustomerPhoneNumberExist($customerPhone)
    {

        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM customers WHERE phone=:customerPhone");
        $result->execute([
            'customerPhone' => $customerPhone
        ]);

        if ($result->rowCount()) {

            return true;

        } else {
            return false;
        }

    }




    public function isTransactionExist($transactionRef){

        $db = $this->getConnection();

        $result = $db->prepare("SELECT * FROM transactions WHERE transactionRef=:transactionRef");
        $result->execute([
            'transactionRef' => $transactionRef
        ]);


        if ($result->rowCount()) {
            return true;
        }else{
            return false;
        }


    }











    public function insertTransactionDetails($customersFk, $bcodeFk, $leaseFk, $channelSubcategoryCode, $transactionRef, $transactionAmount, $transactionDate)
    {
        $db = $this->getConnection();
        $result = $db->prepare("INSERT INTO transactions SET customerFk=:customerFk,bcodeFk=:bcodeFk,leaseFk=:leaseFk,channelSubcategoryCode=:channelSubcategoryCode,
                                transactionRef=:transactionRef,transactionAmount=:transactionAmount,transactionDate=:transactionDate,isUsed=:isUsed");

        $result->execute([
                'customerFk' => $customersFk,
                'bcodeFk' => $bcodeFk,
                'leaseFk' => $leaseFk,
                'channelSubcategoryCode' => $channelSubcategoryCode,
                'transactionRef' => $transactionRef,
                'transactionAmount' => $transactionAmount,
                'transactionDate' => $transactionDate,
                'isUsed' => false
            ]);


      if ($result){
          return true;
      }else{
          return false;
      }

    }







    public function isTransactionvalidated($lease, $transactionRef)
    {

        $db = $this->getConnection();
        $apiClientObj = new ApiClient();
        $repoObj = new Repo();

       // $oneWeekFromNowDate = date('Y-m-d', strtotime('+1 week'));

        $nowDateTime = new DateTime('NOW');
        $nowDateTime = $nowDateTime->format('Y-m-d');

        $result = $db->prepare("SELECT * FROM bcode_lease_log WHERE lease=:lease AND leaseEndDate >=:nowDateTime");
        $result->execute([
            'lease' => $lease,
            'nowDateTime'=>$nowDateTime
        ]);


        if ($result->rowCount())
            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {
                $leaseId = $rows['id'];


                $result = $db->prepare("SELECT * FROM transactions WHERE leaseFk=:leaseId AND transactionRef=:transactionRef AND isUsed=:bool");
                $result->execute([
                    'leaseId' => $leaseId,
                    'transactionRef' => $transactionRef,
                    'bool' => false
                ]);


                if ($result->rowCount())
                    while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {

                        $transRef = $rows['transactionRef'];
                        $tranAmount = $rows['transactionAmount'];
                        $transDate = $rows['transactionDate'];


                        $arr = array($transRef, $tranAmount, $transDate);


                        return $arr;


                    }else{
                    return false;
                }


            }else{



            $result = $db->prepare("SELECT * FROM transactions WHERE transactionRef=:transactionRef AND isUsed=:bool");
            $result->execute([
                'transactionRef' => $transactionRef,
                'bool' => false
            ]);


            if ($result->rowCount()) {
                while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                    $bcodeFk = $rows['bcodeFk'];


                $result = $db->prepare("SELECT * FROM bcode WHERE id=:bcodeFk");
                $result->execute([
                    'bcodeFk' => $bcodeFk
                ]);


                if ($result->rowCount())
                    while ($rows = $result->fetch(PDO::FETCH_ASSOC))
                        $bcodeRef = $rows['bcodeRef'];



                //check if there is any valid lease for customer and if none exist for customer, issue a new lease

                 $result =$this->getCustomerLease($bcodeFk,$bcodeRef);
                 //$lease = $result[0];
                 $leaseId = $result[1];



                    // store the lease Id as a passing_lease in the transactions table against customer
                $result = $db->prepare("UPDATE transactions SET passingLeaseFk=:leaseId WHERE transactionRef=:transactionRef");
                $result->execute([
                    'leaseId' => $leaseId,
                    'transactionRef' => $transactionRef
                ]);





                $result = $db->prepare("SELECT * FROM transactions  WHERE transactionRef=:transactionRef");
                $result->execute([
                    'transactionRef' => $transactionRef
                ]);


                if ($result->rowCount())
                    while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {
                        $transRef = $rows['transactionRef'];
                        $tranAmount = $rows['transactionAmount'];
                        $transDate = $rows['transactionDate'];


                        $arr = array($transRef, $tranAmount, $transDate);


                        return $arr;


                    } else {
                    return false;
                }

            }else{
                return false;
            }



        }

    }





public function getCustomerLease($bcodeFk,$bcodeRef){

    $db = $this->getConnection();
    $apiClientObj = new ApiClient();
    $repoObj = new Repo();

    //check if there is any valid lease for customer before issuing new lease
    $nowDateTime = new DateTime('NOW');
    $nowDateTime = $nowDateTime->format('Y-m-d');
    $result = $db->prepare("SELECT * FROM bcode_lease_log WHERE bcodeFk=:bcodeFk AND leaseEndDate >=:nowDateTime");

    $result->execute([
        'bcodeFk' => $bcodeFk,
        'nowDateTime'=>$nowDateTime
    ]);


    if ($result->rowCount()) {
        while ($rows = $result->fetch(PDO::FETCH_ASSOC))
            $lease = $rows['lease'];
    }else {


        //lease does not exist, requesting for a new lease for the customer
        $APIresponse = $apiClientObj->requestBcodeLease($bcodeRef, $repoObj->numberOfRequiredLease());

        //Inserting Lease
        $generationDate = new DateTime('NOW');
        $generationDate = $generationDate->format('Y-m-d H:i:s');

        $lease = $APIresponse['leaseCode'];
        $leaseStartDate = $APIresponse['leaseStartDate'];
        $leaseEndDate = $APIresponse['leaseEndDate'];
        $this->inserLease($lease, $bcodeFk, $generationDate, $leaseStartDate, $leaseEndDate);
    }




    // get the Id of the lease
    $result = $db->prepare("SELECT * FROM bcode_lease_log WHERE lease=:lease ");
    $result->execute([
        'lease' => $lease,
    ]);


    if ($result->rowCount()) {
        while ($rows = $result->fetch(PDO::FETCH_ASSOC))
            $leaseId = $rows['id'];

        return $arr =array($lease,$leaseId);


    }

}






public function isCustomerLeaseMorethanOneWeekValid($bcodeRef){

    $db = $this->getConnection();
    $result = $db->prepare("SELECT * FROM bcode  WHERE bcodeRef=:bcodeRef");
    $result->execute([
        'bcodeRef' => $bcodeRef
    ]);

    if ($result->rowCount()) {
        while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {

            $id = $rows['id'];
            $oneWeekFromNowDate = date('Y-m-d', strtotime('+1 week'));
            $result = $db->prepare("SELECT * FROM bcode_lease_log WHERE bcodeFk=:bcodeFk AND leaseEndDate >=:oneWeekFromNowDate");

            $result->execute([
                'bcodeFk' => $id,
                'oneWeekFromNowDate' => $oneWeekFromNowDate
            ]);


            if ($result->rowCount()) {
                return true;
            } else {
                return false;
            }


        }
        return true;

    }else{
        return false;
    }
}







    public function updateStatusOfTransactionToUsed($transactionRef){
        $db = $this->getConnection();
        $nowDateTime = new DateTime('NOW');
        $nowDateTime = $nowDateTime->format('Y-m-d H:i:s');

        $result = $db->prepare("UPDATE transactions SET isUsed=:bool, dateUsed=:nowDateTime WHERE transactionRef=:transactionRef");
        $result->execute([
            'bool' => true,
            'nowDateTime'=> $nowDateTime,
            'transactionRef' => $transactionRef
        ]);


    }








    public function isProductSubcategoryCodeValid($channelCategoryFk, $channelSubcategoryCode){


        $db = $this->getConnection();
        $result = $db->prepare("SELECT * FROM channel_sub_category WHERE channelCategoryFk=:channelCategoryFk AND channelSubcategoryCode=:channelSubcategoryCode ");
        $result->execute([
            'channelCategoryFk' => $channelCategoryFk,
            'channelSubcategoryCode' => $channelSubcategoryCode
        ]);

        if($result->rowCount()){
            return true;
        }else{
            return false;
        }
    }





    public function apiActivityLoggin($endPoint, $request, $response){
        $db = $this->getConnection();
        $activityDate = new DateTime('NOW');
        $activityDate = $activityDate->format('Y-m-d H:i:s');

        $result = $db->prepare("INSERT INTO api_activity_logs SET endPoint=:endPoint, request=:request, response=:response, activityDate=:activityDate");
        $result->execute([
            'endPoint' => $endPoint,
            'request' => $request,
            'response'=> $response,
            'activityDate'=> $activityDate
        ]);

        if($result){

            return true;
        }else{

            $msg="Unable to Insert API activity loggin into DB, Query:-"." ".$result;
            return $msg;
        }

    }



    public function bcodeApiActivityLoggin($endPoint, $request, $response){

        $db = $this->getConnection();
        $activityDate = new DateTime('NOW');
        $activityDate = $activityDate->format('Y-m-d H:i:s');

        $result = $db->prepare("INSERT INTO bcode_api_activity_logs SET endPoint=:endPoint, request=:request, response=:response, activityDate=:activityDate");
        $result->execute([
            'endPoint' => $endPoint,
            'request' => $request,
            'response'=> $response,
            'activityDate'=> $activityDate
        ]);

        if($result){

            return true;
        }else{

            $msg="Unable to Insert API activity loggin into DB, Query:-"." ".$result;
            return $msg;
        }

    }


}




?>