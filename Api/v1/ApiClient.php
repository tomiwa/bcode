<?php
/**
 * Created by PhpStorm.
 * User: Tomiwa
 * Date: 9/28/2016
 * Time: 12:30 AM
 */

//include "__.php";

class ApiClient extends __{



    public function genericCurlHeaders($service_url,$curl_post_data){
        $u =$this->firstCred();
        $p =$this->secondCred();
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "$u:$p");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);
        curl_close($curl);

        return $curl_response;
    }


    function isAPIavailible($EndPoint)
    {
        $u =$this->firstCred();
        $p =$this->secondCred();

        //initialize curl
        $service_url = $EndPoint."/api/issuer";
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "$u:$p");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        //get answer
        $response = curl_exec($curl);
        curl_close($curl);

        if ($response) {
            return true;

        }else {
            return false;
        }
    }



    public function requestNewBcode($customerNumber,$message)
    {
        
        $repoObj = new Repo();
        
        $rEndPoint=$this->rootEndPoint();
        $service_url = $rEndPoint."/api/issuer";
        $testDataBool = $this->bCodeRequestDataBoolean();

        $curl_post_data= "<data><type>leased</type><gatewaynumber>SharedUK</gatewaynumber><msisdn>$customerNumber</msisdn><message>$message </message><test>$testDataBool</test></data>";

        $curl_response = $this->genericCurlHeaders($service_url,$curl_post_data);

        $xml_response = simplexml_load_string($curl_response);
        $bodyStatusResponse = $xml_response->status;

        if($bodyStatusResponse !=null && $bodyStatusResponse!=0){

            //activity loggin
            $repoObj->bcodeApiActivityLoggin($service_url, $curl_post_data,$curl_response);
            return false;

        }elseif($bodyStatusResponse !=null && $bodyStatusResponse==0){
            $bCodeRef = $xml_response->{'bcode'}['ref'];
            $smsRef = $xml_response->{'sms'}['ref'];
            $subScriberRef = $xml_response->{'subscriber'}['ref'];


                $bCodeResponseArr = array(
                    'bcodeRef'=>$bCodeRef,
                    'smsRef'=>$smsRef,
                    'subscriberRef'=>$subScriberRef

                );

                //activity loggin
                $repoObj->bcodeApiActivityLoggin($service_url, $curl_post_data, $curl_response);



            return  $bCodeResponseArr;

        }




    }




    public function requestBcodeLease($bCodeRef, $noOfLease) {
        $repoObj = new Repo();

        $rEndPoint=$this->rootEndPoint();
        $service_url = $rEndPoint."/api/bcode/".$bCodeRef."/lease";
        $curl_post_data= "<data><numberOfLeases>$noOfLease</numberOfLeases></data>";

        $curl_response = $this->genericCurlHeaders($service_url,$curl_post_data);
      //  $array_data = json_decode(json_encode(simplexml_load_string($curl_response)), true);


        $xml_response = simplexml_load_string($curl_response);

        $bodyStatusResponse = $xml_response->status;

        if($bodyStatusResponse !=null && $bodyStatusResponse!=0){

            $repoObj->bcodeApiActivityLoggin($service_url, $curl_post_data, $curl_response);
            return false;

        }elseif($bodyStatusResponse !=null && $bodyStatusResponse==0){

            $leaseStartDate= $xml_response->leases->{'lease'}['startTS'];
            $leaseEndDate = $xml_response->leases->{'lease'}['endTS'];
            $leaseCode = $xml_response->leases->{'lease'}['hmac'];

            $leaseStartDate= new DateTime($leaseStartDate);
            $leaseStartDate=$leaseStartDate->format('Y-m-d');
            $leaseEndDate= new DateTime($leaseEndDate);
            $leaseEndDate=$leaseEndDate->format('Y-m-d');




            $leaseResponseArr = array(
                'leaseCode'=>$leaseCode,
                'leaseStartDate'=>$leaseStartDate,
                'leaseEndDate'=>$leaseEndDate

            );


            //activity loggin
            $repoObj->bcodeApiActivityLoggin($service_url, $curl_post_data, $curl_response);
            return $leaseResponseArr;

        }

    }



    public function retrieveBcodeForCustomer($bcodeRef, $customerPhone){
        $repoObj = new Repo();
        $rEndPoint=$this->rootEndPoint();
        $service_url = $rEndPoint."/api/sms";

        $curl_post_data= "<data><bcodeid>$bcodeRef</bcodeid><gatewaynumber>SharedUK</gatewaynumber><msisdn>$customerPhone</msisdn><message>{{bcode}} Above is your retrieved bCODE </message></data>";

        $curl_response = $this->genericCurlHeaders($service_url,$curl_post_data);

        $xml_response = simplexml_load_string($curl_response);
        $bodyStatusResponse = $xml_response->status;

        if($bodyStatusResponse !=null && $bodyStatusResponse!=0){

            //activity loggin
            $repoObj->bcodeApiActivityLoggin($service_url, $curl_post_data,$curl_response);
            return false;

        }elseif($bodyStatusResponse !=null && $bodyStatusResponse==0){

            $smsRef = $xml_response->{'sms'}['ref'];
            $responseMsg = "bcode successfully resent to customer";


            $responseArr = array(
                'code'=>200,
                'status'=>'true',
                'responseMsg'=>$responseMsg,
                'smsRef'=>$smsRef
            );

            //activity loggin
            $repoObj->bcodeApiActivityLoggin($service_url, $curl_post_data, $curl_response);
            return  $responseArr;

        }

    }




public function sendSMS($customerPhone, $message){

    $repoObj = new Repo();

    $rEndPoint=$this->rootEndPoint();
    $service_url = $rEndPoint."/api/sms";

    $curl_post_data= "<data><gatewaynumber>SharedUK</gatewaynumber><msisdn>$customerPhone</msisdn><message>{{}}$message </message></data>";


    $curl_response = $this->genericCurlHeaders($service_url,$curl_post_data);
    $xml_response = simplexml_load_string($curl_response);
    $bodyStatusResponse = $xml_response->status;


    if($bodyStatusResponse !=null && $bodyStatusResponse!=0){

        //activity loggin
        $repoObj->bcodeApiActivityLoggin($service_url, $curl_post_data, $curl_response);
        return false;

    }elseif($bodyStatusResponse !=null && $bodyStatusResponse==0){

        $smsRef = $xml_response->{'sms'}['ref'];
        $subScriberRef = $xml_response->{'subscriber'}['ref'];



        $responseArr = array(
            'smsRef'=>$smsRef,
            'subScriberRef'=>$subScriberRef

        );

        //activity loggin
        $repoObj->bcodeApiActivityLoggin($service_url, $curl_post_data, $curl_response);
        return  $responseArr;

    }

}





}